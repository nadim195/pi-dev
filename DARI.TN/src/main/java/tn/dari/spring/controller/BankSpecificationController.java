package tn.dari.spring.controller;

import tn.dari.spring.domain.BankSpecification;
import tn.dari.spring.service.BankSpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/bank")
public class BankSpecificationController {

    @Autowired
    private BankSpecificationService bankspecificationService;

    @PostMapping("/")
    public BankSpecification save(@RequestBody BankSpecification bankspecification) {
        return bankspecificationService.save(bankspecification);
    }

    @GetMapping("/{id}")
    public Optional<BankSpecification> getById(@PathVariable(value = "id") Long id) {
        return bankspecificationService.find(id);
    }

    @PutMapping("/{id}")
    public BankSpecification update(@PathVariable(value = "id") Long id, @RequestBody BankSpecification bankspecification) {
        bankspecification.setId(id);
        return bankspecificationService.save(bankspecification);
    }


    @GetMapping("/")
    public List<BankSpecification> getAll() {
        return bankspecificationService.findAll();
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable(value = "id") Long id) {
        bankspecificationService.delete(id);
    }

    @GetMapping("/count")
    public long count() {
        return bankspecificationService.count();
    }
}
