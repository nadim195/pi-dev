package tn.dari.spring.controller;

import java.time.Instant;
import java.time.Period;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AnnouncementType;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Documents;
import tn.dari.spring.domain.PriceHistory;
import tn.dari.spring.domain.Profile;
import tn.dari.spring.domain.SaleHistory;
import tn.dari.spring.domain.util.AppUserForm;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.CriteriaRepository;
import tn.dari.spring.repository.DocumentsRepository;
import tn.dari.spring.repository.PriceHistoryRepository;
import tn.dari.spring.repository.ProfileRepository;
import tn.dari.spring.repository.RentHistoryRepository;
import tn.dari.spring.repository.SaleHistoryRepository;
import tn.dari.spring.service.interfaces.AnnouncementService;
import tn.dari.spring.service.interfaces.AppUserService;
import tn.dari.spring.service.interfaces.DocumentsService;
import tn.dari.spring.service.interfaces.FavouriteAnnoucementService;

@RestController
@RequestMapping("/selltest")
public class SellTestController {
	
	@Autowired
	FavouriteAnnoucementService favService;
	@Autowired
	AnnouncementService announcementService;
	@Autowired
	AppUserService userService;
	@Autowired
	ProfileRepository profRep;
	@Autowired
	DocumentsService docService;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	AppUserRepository userRep;
	@Autowired
	RentHistoryRepository rentRep;
	@Autowired
	CriteriaRepository critRep;
	@Autowired
	DocumentsRepository docRep;
	@Autowired
	PriceHistoryRepository phRep;
	@Autowired
	SaleHistoryRepository saleRep;
	
	
	@GetMapping("/get_favorites")
	public List<Announcement> getFavotriteAnnouncements(@RequestParam("id") int userId){
		return favService.fetchFavouriteAnnouncements(userId).stream().collect(Collectors.toList());
	}
	@DeleteMapping("/deleteann")
	public void deleteAnnouncement(@RequestParam("id")  int announcementId){
		announcementService.DeleteAnnouncement(announcementId);
	}
	@PutMapping("/updateann")
	public Announcement UpdateAnnouncement(@RequestBody Announcement announcement,@RequestParam("announcement_id") int announcementId){
		announcementService.Update(announcement,announcementId);
		return announcement;
	}
	@PostMapping("/addann")
	public Announcement AddAnnouncement(@RequestBody Announcement announcement, @RequestParam("user_id") int userId){
		announcementService.AddAnnouncement(announcement,userId);
		return announcement;
	}
	@GetMapping("/getallann")
	public List<Announcement> getAllAnnouncement(){
		return announcementService.loadAllAnnouncements();
	}
	@GetMapping("/get_my_announcements")
	public List<Announcement> getAllAnnouncementByPoster(@RequestParam("user_id") int userId){
		return announcementService.loadAllActive().stream().filter(a -> a.getUser().getUserId()==userId).collect(Collectors.toList());
	}
	
	@DeleteMapping("/deleteall")
	public int deleteAll(){
		//annRep.deleteAll();
		userRep.deleteAll();
		//rentRep.deleteAll();
		//critRep.deleteAll();
		//docRep.deleteAll();
		//saleRep.deleteAll();
		//phRep.deleteAll();
		//profRep.deleteAll();
		return 1;
	}
	
	@PostMapping("/valid")
	public int validation(){
		userRep.deleteAll();
		
		AppUserForm client = new AppUserForm();
		client.setFirstName("client1");
		client.setEmail("client1");
		client.setPassword("client1");
		client.setVerified(true);
		AppUser clientu = userService.addSimpleUser(client);

		AppUserForm owner = new AppUserForm();
		owner.setFirstName("owner1");
		owner.setEmail("owner1");
		owner.setPassword("owner1");
		owner.setVerified(true);
		AppUser owneru = userService.addSimpleUser(owner);

		Documents documents = new Documents("payslip.pdf", "idCard.pdf", "engagementLetter.pdf", "depositProof.pdf");
		docService.add(documents, clientu.getUserId());
		
		AppUserForm client2 = new AppUserForm();
		client2.setFirstName("client2");
		client2.setEmail("client2");
		client2.setPassword("client2");
		client2.setVerified(true);
		AppUser clientu2 = userService.addSimpleUser(client2);

		AppUserForm owner2 = new AppUserForm();
		owner2.setFirstName("owner2");
		owner2.setEmail("owner2");
		owner2.setPassword("owner2");
		owner2.setVerified(true);
		AppUser owneru2 = userService.addSimpleUser(owner2);

		Documents documents2 = new Documents("payslip2.pdf", "idCard2.pdf", "engagementLetter2.pdf", "depositProof2.pdf");
		docService.add(documents2, clientu2.getUserId());
		
		Announcement announcement1 = new Announcement(AnnouncementType.Sale, "estateType1", "location1", 20000, "description", 300, 2, 4);
		Announcement announcement2 = new Announcement(AnnouncementType.HolidayRent, "estateType2", "location1", 200, "description", 600, 3, 5);
		Announcement announcement3 = new Announcement(AnnouncementType.Rent, "estateType2", "location1", 800, "description", 500, 2, 3);
		Announcement announcement4 = new Announcement(AnnouncementType.Rent, "estateType1", "location2", 500, "description", 400, 1, 3);
		Announcement announcement5 = new Announcement(AnnouncementType.Sale, "estateType1", "location2", 45000, "description", 1000, 3, 5);
		Announcement announcement6 = new Announcement(AnnouncementType.HolidayRent, "estateType1", "location2", 200, "description", 300, 1, 2);
		Announcement announcement7 = new Announcement(AnnouncementType.Rent, "estateType2", "location4", 400, "description", 100, 1, 1);
		Announcement announcement8 = new Announcement(AnnouncementType.Rent, "estateType2", "location5", 1000, "description", 250, 1, 2);
		Announcement announcement9 = new Announcement(AnnouncementType.Rent, "estateType3", "location3", 600, "description", 750, 2, 3);
		Announcement announcement = new Announcement(AnnouncementType.Sale, "estateType3", "location3", 32000, "description", 600, 2, 3);
		
		announcementService.AddAnnouncement(announcement1, owneru.getUserId());
		announcementService.AddAnnouncement(announcement2, owneru2.getUserId());
		announcementService.AddAnnouncement(announcement3, owneru.getUserId());
		announcementService.AddAnnouncement(announcement4, owneru2.getUserId());
		announcementService.AddAnnouncement(announcement5, owneru.getUserId());
		announcementService.AddAnnouncement(announcement6, owneru.getUserId());
		announcementService.AddAnnouncement(announcement7, owneru.getUserId());
		announcementService.AddAnnouncement(announcement8, owneru2.getUserId());
		announcementService.AddAnnouncement(announcement9, owneru.getUserId());
		announcementService.AddAnnouncement(announcement, owneru.getUserId());
		
		PriceHistory history1 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(27))), 300, announcement6);
		phRep.save(history1);
		PriceHistory history2 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(25))), 400, announcement6);
		phRep.save(history2);
		PriceHistory history3 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(23))), 500, announcement6);
		phRep.save(history3);
		PriceHistory history4 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(21))), 400, announcement6);
		phRep.save(history4);
		PriceHistory history5 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(19))), 350, announcement6);
		phRep.save(history5);
		PriceHistory history6 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(17))), 300, announcement6);
		phRep.save(history6);
		PriceHistory history7 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(15))), 600, announcement6);
		phRep.save(history7);
		PriceHistory history8 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(11))), 500, announcement6);
		phRep.save(history8);
		PriceHistory history9 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(10))), 450, announcement6);
		phRep.save(history9);
		PriceHistory history10 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(9))), 150, announcement6);
		phRep.save(history10);
		PriceHistory history11 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(7))), 200, announcement6);
		phRep.save(history11);
		PriceHistory history12 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(6))), 280, announcement6);
		phRep.save(history12);
		PriceHistory history13 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(4))), 354, announcement6);
		phRep.save(history13);
		PriceHistory history14 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(2))), 258, announcement6);
		phRep.save(history14);
		PriceHistory history15 = new PriceHistory(Date.from(Instant.now().minus(Period.ofDays(1))), 200, announcement6);
		phRep.save(history15);
		
		favService.addToFavourite(clientu.getUserId(), announcement6.getAnnouncementId());
		
		return 1;
	}
	
	/*@Autowired
	SaleHistoryService saleService;
	@Autowired
	CriteriaService criteriaService;
	@Autowired
	DocumentsService documentsService;
	@Autowired
	RentHistoryService rentService;
	@Autowired
	InterestService interestService;
	@Autowired
	PriceHistoryService priceService;
	@Autowired
	ZonePriceService zoneService;
	@Autowired
	NotificationService notificationService;
	
	@Autowired
	AnnouncementService announcementService;
	@Autowired
	AppUserRepository userRep;
	
	@PostMapping("/addnotif")
	public Notification AddNotification(@RequestBody Notification notification,@RequestParam("announce") int announcementId,@RequestParam("profile") int profileId){		
		notificationService.AddNotification(notification,announcementId,profileId);	
		return notification;
	}
	@PostMapping("/addann")
	public Announcement AddAnnouncement(@RequestBody Announcement announcement, @RequestParam("id") int userId){
		announcementService.AddAnnouncement(announcement, userId);
		return announcement;
	}
	@PostMapping("/addsale")
	public SaleHistory AddSaleHistory(@RequestBody SaleHistory saleHistory, @RequestParam("announceId") int announcementId){
		saleService.AddSaleHistory(saleHistory,announcementId);
		return saleHistory;
	}
	@PostMapping("/addcriteria")
	public Criteria AddCriteria(@RequestBody Criteria criteria){
		criteriaService.AddCriteria(criteria);
		return criteria;
	}
	@PostMapping("/adddocuments")
	public Documents AddDocuments(@RequestBody Documents documents){
		documentsService.AddDocuments(documents);
		return documents;
	}
	@PostMapping("/addrent")
	public RentHistory AddRentHistory(@RequestBody RentHistory rentHistory, @RequestParam("announceId") int announcementId){
		rentService.AddRentHistory(rentHistory,announcementId);
		return rentHistory;
	}
	@PostMapping("/addinterest")
	public Interest AddInterest(@RequestBody Interest interest, @RequestParam("profile") int profileId){
		interestService.AddInterest(interest,profileId);
		return interest;
	}
	@PostMapping("/addprice")
	public double AddPriceHistory(@RequestBody PriceHistory priceHistory, @RequestParam("announceId") int announcementId){
		return priceService.AddPriceHistory(priceHistory,announcementId);
	}
	@PostMapping("/addzone")
	public ZonePrice AddZonePrice(@RequestBody ZonePrice zonePrice){
		zoneService.AddZonePrice(zonePrice);
		return zonePrice;
	}
	
	@GetMapping("/getallnotif")
	public List<Notification> getAllNotification(){
		return notificationService.fetchAllNotification();
	}
	@GetMapping("/getallannounc")
	public List<Announcement> getAllAnnouncement(){
		return announcementService.loadAllAnnouncements();
	}
	@GetMapping("/getallsale")
	public List<SaleHistory> getAllSaleHistory(){
		return saleService.fetchAllSaleHistory();
	}
	@GetMapping("/getallrent")
	public List<RentHistory> getAllRentHistory(){
		return rentService.fetchAllRentHistory();
	}
	@GetMapping("/getallpricehistory")
	public List<PriceHistory> getAllPriceHistory(){
		return priceService.fetchAllPriceHistory();
	}
	@GetMapping("/getallzone")
	public List<ZonePrice> getAllZonePrice(){
		return zoneService.fetchAllZonePrice();
	}
	@GetMapping("/getallinterest")
	public List<Interest> getAllInterests(){
		return interestService.fetchAllInterest();
	}
	@GetMapping("/getalldocs")
	public List<Documents> getAllDocuments(){
		return documentsService.fetchAllDocuments();
	}
	@GetMapping("/getallcriteria")
	public List<Criteria> getAllCriteria(){
		return criteriaService.fetchAllCriteria();
	}
	
	@DeleteMapping("/deleteann")
	public void deleteAnnouncement(@RequestParam("id")  int announcementId){
		announcementService.DeleteAnnouncement(announcementId);
	}
	@DeleteMapping("/deleteph")
	public void deletePriceHistory(@RequestParam("id")  int priceHistoryId){
		priceService.DeletePriceHistoryById(priceHistoryId);
	}
	@DeleteMapping("/deletenotif")
	public void deleteNotification(@RequestParam("id")  int notificationId){
		notificationService.DeleteNotificationById(notificationId);
	}
	@DeleteMapping("/deletesh")
	public void deleteSaleHistory(@RequestParam("id")  int saleHistoryId){
		saleService.DeleteSaleHistoryById(saleHistoryId);
	}
	@DeleteMapping("/deleterh")
	public void deleteRentHistory(@RequestParam("id")  int rentHistoryId){
		rentService.DeleteRentHistoryById(rentHistoryId);
	}
	@DeleteMapping("/deletecrit")
	public void deleteCriteria(@RequestParam("id")  int criteriaId){
		criteriaService.DeleteCriteriaById(criteriaId);
	}
	@DeleteMapping("/deletezone")
	public void deleteZonePrice(@RequestParam("id") int zonePriceId){
		zoneService.DeleteZonePriceById(zonePriceId);
	}
	@DeleteMapping("/deleteinterest")
	public void deleteInterest(@RequestParam("id") int interestId){
		interestService.DeleteInterestById(interestId);
	}
	@DeleteMapping("/deletedocs")
	public void deleteDocuments(@RequestParam("id") int documentsId){
		documentsService.DeleteDocumentsById(documentsId);
	}
	
	@PutMapping("/updateannonce")
	public Announcement UpdateAnnouncement(@RequestBody Announcement announcement,@RequestParam("id") int announcementId){
		announcementService.Update(announcement,announcementId);
		return announcement;
	}
	@PutMapping("/updatenotif")
	public Notification UpdateNotification(@RequestBody Notification notification,@RequestParam("id") int notificationId){		
		notificationService.UpdateNotification(notification, notificationId);	
		return notification;
	}
	@PutMapping("/updatesale")
	public SaleHistory UpdateSaleHistory(@RequestBody SaleHistory saleHistory, @RequestParam("id") int saleHistoryId){
		saleService.UpdateSaleHistory(saleHistory, saleHistoryId);
		return saleHistory;
	}
	@PutMapping("/updatecriteria")
	public Criteria UpdateCriteria(@RequestBody Criteria criteria, @RequestParam("id") int criteriaId){
		criteriaService.UpdateCriteria(criteria, criteriaId);
		return criteria;
	}
	@PutMapping("/updatedocuments")
	public Documents UpdateDocuments(@RequestBody Documents documents, @RequestParam("id") int documentsId){
		documentsService.UpdateDocuments(documents, documentsId);
		return documents;
	}
	@PutMapping("/updaterent")
	public RentHistory UpdateRentHistory(@RequestBody RentHistory rentHistory, @RequestParam("id") int rentHistoryId){
		rentService.UpdateRentHistory(rentHistory, rentHistoryId);
		return rentHistory;
	}
	@PutMapping("/updateinterest")
	public Interest UpdateInterest(@RequestBody Interest interest, @RequestParam("id") int interestId){
		interestService.UpdateInterest(interest, interestId);
		return interest;
	}
	@PutMapping("/updateprice")
	public PriceHistory UpdatePriceHistory(@RequestBody PriceHistory priceHistory, @RequestParam("id") int priceHistoryId){
		return priceService.UpdatePriceHistory(priceHistory, priceHistoryId);
	}
	@PutMapping("/updatezone")
	public ZonePrice UpdateZonePrice(@RequestBody ZonePrice zonePrice, @RequestParam("id") int zonePriceId){
		zoneService.UpdateZonePrice(zonePrice, zonePriceId);
		return zonePrice;
	}*/

}
