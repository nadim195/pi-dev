package tn.dari.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.SaleHistory;
import tn.dari.spring.service.interfaces.SaleHistoryService;

@RestController
@RequestMapping("/sale")
public class SaleController {
	
	@Autowired
	SaleHistoryService saleService;
	
	@PostMapping("/add")
	public SaleHistory add(@RequestBody SaleHistory saleHistory, @RequestParam("announcement_id") int announcementId){
		saleService.add(saleHistory,announcementId);
		return saleHistory;
	}
	@GetMapping("/getall")
	public List<SaleHistory> getAll(){
		return saleService.fetchAll();
	}
	@GetMapping("/get")
	public SaleHistory getSaleHistory(@RequestParam("id") int id){
		return saleService.fetchOne(id);
	}
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id")  int id){
		saleService.deleteById(id);
	}
	@PutMapping("/update")
	public SaleHistory update(@RequestBody SaleHistory saleHistory, @RequestParam("id") int id){
		saleService.update(saleHistory, id);
		return saleHistory;
	}
	
}
