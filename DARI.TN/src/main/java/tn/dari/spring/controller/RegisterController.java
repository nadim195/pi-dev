package tn.dari.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.ConfirmationToken;
import tn.dari.spring.domain.util.AppUserForm;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.ConfirmationTokenRepository;
import tn.dari.spring.service.EmailSenderService;
import tn.dari.spring.service.interfaces.AnnouncementService;
import tn.dari.spring.service.interfaces.AppUserService;
import tn.dari.spring.service.interfaces.TestService;

@RestController
@RequestMapping("/registerApi")
public class RegisterController {
	
	@Autowired
	TestService service;
	
	@Autowired
	AppUserService appUserService;

	@Autowired
	AnnouncementService annService;
	@Autowired
	private ConfirmationTokenRepository confirmationTokenRepository;
	@Autowired
	private EmailSenderService emailSenderService;
	@Autowired
	private AppUserRepository userRep;
	
	@PostMapping("/Signup")
	public AppUser register(@RequestBody AppUserForm appUser) {
		AppUser user = appUserService.addSimpleUser(appUser);
		ConfirmationToken confirmationToken = new ConfirmationToken(user);

		confirmationTokenRepository.save(confirmationToken);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(appUser.getEmail());
		mailMessage.setSubject("Complete Registration!");
		mailMessage.setFrom("daritn2021@gmail.com");
		mailMessage.setText("To confirm your account, please click here : "
				+ "http://localhost:8081/registerApi/confirm-account?token=" + confirmationToken.getConfirmationToken());
		emailSenderService.sendEmail(mailMessage);
		return user;
	}

	@RequestMapping(value = "confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
	public String confirmUserAccount(@RequestParam("token") String confirmationToken) {
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

		if (token != null) {
			AppUser user = userRep.findByEmail(token.getUser().getEmail());
			AppUserForm af = new AppUserForm();
			af.setEmail(user.getEmail());
			af.setVerified(true);
			appUserService.updateUser(af);
			return "your email was verified successfully "+user.getLastName()+" "+user.getFirstName() +" , you can now Log In to your account" ;
		}
		return "failed to recognize your verification token";

	}


}
