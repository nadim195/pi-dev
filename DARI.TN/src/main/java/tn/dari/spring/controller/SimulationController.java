package tn.dari.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.dari.spring.dto.CreditDTO;
import tn.dari.spring.dto.SimulatePayload;
import tn.dari.spring.service.SimulationService;

import java.util.List;

@RestController
@RequestMapping("simulation")
public class SimulationController {

    @Autowired
    SimulationService simulationService;

    @PostMapping("simulate")
    public ResponseEntity<List<CreditDTO>> simulate(@RequestBody SimulatePayload payload){
        return ResponseEntity.ok(simulationService.simulate(payload));
    }

}
