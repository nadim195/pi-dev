package tn.dari.spring.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.dari.spring.domain.Interest;
import tn.dari.spring.domain.util.InterestDocuments;
import tn.dari.spring.service.interfaces.InterestService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/interest")
public class InterestController {
	
	@Autowired
	InterestService interestService;
	
	@PostMapping("/declare")
	public int declare(@RequestParam("announcement_id") int announcementId,@RequestParam("user_id") int userId){
		return interestService.declare(announcementId,userId);
	}
	@GetMapping("/getall")
	public List<Interest> getAll(){
		return interestService.fetchAll();
	}
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id") int id){
		interestService.deleteById(id);
	}
	@DeleteMapping("/reject")
	public void reject(@RequestParam("id") int id){
		interestService.reject(id);
	}
	@PutMapping("/update")
	public Interest update(@RequestBody Interest interest, @RequestParam("id") int id){
		interestService.update(interest, id);
		return interest;
	}
	@GetMapping("/getby_announcement2")
	public List<Interest> getAllByAnnouncement2(@RequestParam("announcement_id") int announcementId){
		return interestService.fetchAllByAnnouncementId(announcementId);
	}
	@GetMapping("/getby_announcement")
	public List<InterestDocuments> getAllByAnnouncement(@RequestParam("announcement_id") int announcementId){
		return interestService.fetchAllByAnnouncementId(announcementId).stream().map(i -> interestService.getInterestDocuments(i)).collect(Collectors.toList());
		
	}
	@PutMapping("/accept")
	public void accept(@RequestParam("id") int id){
		interestService.accept(id);
	}
	@GetMapping("/get_accepted2")
	public List<Interest> getAllAcceptedByPoster2(@RequestParam("user_id") int userId){
		return interestService.fetchAllAcceptedByPoster(userId);
	}
	@GetMapping("/get_accepted")
	public List<InterestDocuments> getAllAcceptedByPoster(@RequestParam("user_id") int userId){
		return interestService.fetchAllAcceptedByPoster(userId).stream().map(i -> interestService.getInterestDocuments(i)).collect(Collectors.toList());
	}
	
	
	
}
