package tn.dari.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.RentHistory;
import tn.dari.spring.service.interfaces.RentHistoryService;

@RestController
@RequestMapping("/rent")
public class RentController {
	
	@Autowired
	RentHistoryService rentService;
	

	@PostMapping("/add")
	public RentHistory add(@RequestBody RentHistory RentHistory, @RequestParam("announcement_id") int announcementId){
		rentService.add(RentHistory,announcementId);
		return RentHistory;
	}
	@GetMapping("/getall")
	public List<RentHistory> getAll(){
		return rentService.fetchAll();
	}
	@GetMapping("/get")
	public RentHistory getRentHistory(@RequestParam("id") int id){
		return rentService.fetchOne(id);
	}
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id")  int id){
		rentService.deleteById(id);
	}
	@PutMapping("/update")
	public RentHistory update(@RequestBody RentHistory RentHistory, @RequestParam("id") int id){
		rentService.update(RentHistory, id);
		return RentHistory;
	}
	@PutMapping("/rent")
	public RentHistory markRented(@RequestParam("announcement_id") int announcementId){
		return rentService.markRented(announcementId);
	}
	
}
