package tn.dari.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Role;
import tn.dari.spring.domain.util.AppUserForm;
import tn.dari.spring.service.interfaces.AppUserService;
import tn.dari.spring.service.interfaces.FavouriteAnnoucementService;
import tn.dari.spring.service.interfaces.TestService;
import java.util.Set;

@RestController
@RequestMapping()
public class TestController {
	
	@Autowired
	TestService service;
	
	@Autowired 
	AppUserService appUserService;
	@Autowired
	FavouriteAnnoucementService favServ;
	
	
	
	@PostMapping("/addRole")
	public Role addRole(@RequestBody Role r)
	{
		service.addRole(r);
		return r;
	}
	
	@PostMapping("/addSimpleUser")
	public AppUser addSimpleUser(@RequestBody AppUserForm appUser)
	{
		return appUserService.addSimpleUser(appUser);
	
	}
	@PostMapping("/addToFavourite")
	public void addToFavourite(@RequestParam("iduser") int iduser,@RequestParam("idann") int idann)
	{
		 favServ.addToFavourite(iduser, idann);
	}
	@GetMapping("/favourites")
	public Set<Announcement> favs(@RequestParam("iduser") int id)
	{
		return favServ.fetchFavouriteAnnouncements(id);
	}
	@DeleteMapping("/deleteFromFav")
	public void deleteFromFavourite(@RequestParam("iduser") int iduser,@RequestParam("idann") int idann)
	{
		favServ.deleteFromFavourite(iduser, idann);
	}
	@GetMapping("/countUsersToday")
    public int usersToday()
    {
		return appUserService.newSimpleUsersToday();
    }
	


}
