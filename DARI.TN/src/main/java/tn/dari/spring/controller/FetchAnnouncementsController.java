package tn.dari.spring.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.service.interfaces.AnnouncementService;
import tn.dari.spring.service.interfaces.AppUserService;
import tn.dari.spring.service.interfaces.FavouriteAnnoucementService;
import tn.dari.spring.service.interfaces.UserHistoryService;

@RestController
@RequestMapping("/apiHF")
public class FetchAnnouncementsController {
	
	@Autowired
	AnnouncementService annService;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	AppUserService userServ;
	@Autowired
	FavouriteAnnoucementService favServ;
	@Autowired
	UserHistoryService historyServ;
	@GetMapping("/getAnnounces")
	public List<Announcement> getAnns(){
		return annService.loadAllActive();
}
	@GetMapping("/Announce")
	public Optional<Announcement> getAnn(@RequestParam("id") int id,Authentication auth){
		AppUser current=userServ.loadUserByEmail(auth.getName());
		historyServ.addToHistory(current.getUserId(), id);
		return annRep.findById(id);
}
	@PostMapping("/addToFavourite")
	public void addToToFavourite(@RequestParam("id") int id,Authentication auth)
	{
		AppUser current=userServ.loadUserByEmail(auth.getName());
		favServ.addToFavourite(current.getUserId(), id);
	}
	@GetMapping("/history")
	public Set<Announcement> getHistory(Authentication auth)
	{
		AppUser current=userServ.loadUserByEmail(auth.getName());
		return current.getAnnouncementsHistory();
	}
	@GetMapping("/Favourites")
	public Set<Announcement> getFavourites(Authentication auth)
	{
		AppUser current=userServ.loadUserByEmail(auth.getName());
		return current.getFavouritesAnnouncements();
	}
	@PostMapping("/addAnnounce")
	public void addAnnounce(@RequestBody Announcement ann,Authentication auth)
	{
		AppUser current=userServ.loadUserByEmail(auth.getName());
		ann.setArchived(false);
		ann.setVerified(false);
		annService.AddAnnouncement(ann, current.getUserId());
		
	}
	
}
