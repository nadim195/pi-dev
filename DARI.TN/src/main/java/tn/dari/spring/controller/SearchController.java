package tn.dari.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.Criteria;
import tn.dari.spring.service.interfaces.CriteriaService;

@RestController
@RequestMapping("/search")
public class SearchController {

	@Autowired
	CriteriaService criteriaService;
	
	@GetMapping("/filter")
	public List<Announcement> search(@RequestBody Criteria criteria){
		return criteriaService.search(criteria);
	}
	
	@GetMapping("/")
	public List<Announcement> search(){
		return criteriaService.search();
	}
	
	@GetMapping("/filterbyname")
	public List<Announcement> search(@RequestParam("name") String name, @RequestParam("user_id") int userId){
		return criteriaService.search(name,userId);
	}
	
	@GetMapping("/allfilters")
	public List<Announcement> search(@RequestParam("user_id") int userId){
		return criteriaService.search(userId);
	}
	
}
