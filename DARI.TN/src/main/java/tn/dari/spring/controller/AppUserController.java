package tn.dari.spring.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Availability;
import tn.dari.spring.domain.Meeting;
import tn.dari.spring.domain.util.AppUserForm;
import tn.dari.spring.domain.util.MeetingForm;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.service.interfaces.AnnouncementService;
import tn.dari.spring.service.interfaces.AppUserService;
import tn.dari.spring.service.interfaces.TestService;

@RestController
@RequestMapping("/userapi")
public class AppUserController {

	@Autowired
	TestService service;

	@Autowired
	AppUserService appUserService;

	@Autowired
	AnnouncementService annService;
	@Autowired 
	AnnouncementRepository annRep;


	

	@PutMapping("/update")
	public AppUser updateUser(@RequestBody AppUserForm appUser) {
		return appUserService.updateUser(appUser);
	}

	@PostMapping("reportUser")
	public void reportUser(Authentication auth,@RequestParam("reportedUserId") int reportedUserId)
	{
		AppUser current=appUserService.loadUserByEmail(auth.getName());
		appUserService.reportUser(current.getUserId(), reportedUserId);
	}
	@PostMapping("confirmMeeting")
	public Meeting confirmMetting(Authentication auth,@RequestParam("requestUserId") int requestUserId)
	{
		AppUser current=appUserService.loadUserByEmail(auth.getName());
		return appUserService.confirmMeeting(current.getUserId(), requestUserId);
	}
	@PostMapping("requestMeeting")
	public void requestMeeting(Authentication auth,@RequestParam("annId") int annId,@RequestBody MeetingForm meet)
	{
		AppUser current=appUserService.loadUserByEmail(auth.getName());
		AppUser user=annRep.findById(annId).get().getUser();
		appUserService.requestMeeting(current.getUserId(), user.getUserId(),meet);
	}
	@PostMapping("addAvailability")
	public void addAvailability(@RequestBody Availability av,Authentication auth)
	{
		AppUser current=appUserService.loadUserByEmail(auth.getName());
		appUserService.addAvailability(av, current.getUserId());
	}
	@PostMapping("/loggedIn")
	 public String processProducts(Authentication auth)
	{
		return auth.getName();
	}
	@GetMapping("/requestedMeetings")
	public List<Meeting> requestedMeetings(Authentication auth)
	{
		AppUser current=appUserService.loadUserByEmail(auth.getName());
		return appUserService.requestedMeetings(current.getUserId());
	}

}
