package tn.dari.spring.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.service.interfaces.FavouriteAnnoucementService;
import tn.dari.spring.service.interfaces.UserHistoryService;

@RestController
@RequestMapping("/api")
public class FavouriteHistoryController {

	@Autowired
	FavouriteAnnoucementService favServ;
	@Autowired
	UserHistoryService historyServ;

	@PostMapping("/addToFavourite")
	public void addToFavourite(@RequestParam("iduser") int iduser, @RequestParam("idann") int idann) {
		favServ.addToFavourite(iduser, idann);
	}

	@DeleteMapping("/deleteFromFavourite")
	public void deleteFromFavourite(@RequestParam("iduser") int iduser, @RequestParam("idann") int idann) {
		favServ.deleteFromFavourite(iduser, idann);
	}

	@GetMapping("/favourites")
	public Set<Announcement> favourritesAnnoucements(@RequestParam("iduser") int iduser) {
		return favServ.fetchFavouriteAnnouncements(iduser);
	}

	@PostMapping("/addToHistory")
	public void addToHistory(@RequestParam("iduser") int iduser, @RequestParam("idann") int idann) {
		historyServ.addToHistory(iduser, idann);
	}

	@DeleteMapping("/deleteFromHistory")
	public void deleteFromHistory(@RequestParam("iduser") int iduser, @RequestParam("idann") int idann) {
		historyServ.deleteFromHistory(iduser, idann);
	}

	@GetMapping("/history")
	public Set<Announcement> history(@RequestParam("iduser") int iduser) {
		return historyServ.fetchAllhistory(iduser);
	}

}
