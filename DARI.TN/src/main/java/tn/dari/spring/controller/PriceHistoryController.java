package tn.dari.spring.controller;

import java.time.Period;
import java.util.DoubleSummaryStatistics;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.PriceHistory;
import tn.dari.spring.domain.util.AnnouncementInfo;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.service.interfaces.PriceHistoryService;

@RestController
@RequestMapping("/price_history")
public class PriceHistoryController {
	
	@Autowired
	PriceHistoryService priceService;
	@Autowired
	AnnouncementRepository announcementRep;;
	
	@PostMapping("/add")
	public double add(@RequestBody PriceHistory priceHistory, @RequestParam("announcement_id") int announcementId){
		return priceService.add(priceHistory,announcementId);
	}
	@GetMapping("/getall")
	public List<PriceHistory> getAll(){
		return priceService.fetchAll();
	}
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id")  int id){
		priceService.deleteById(id);
	}
	@PutMapping("/update")
	public PriceHistory update(@RequestBody PriceHistory priceHistory, @RequestParam("id") int id){
		return priceService.update(priceHistory, id);
	}
	@GetMapping("/getbyannouncement")
	public List<PriceHistory> getByAnnouncement(@RequestParam("announcement_id") int announcementId){
		return priceService.fetchAllByAnnouncement(announcementRep.findById(announcementId).get());
	}
	@GetMapping("/get_stats")
	public DoubleSummaryStatistics getStats(@RequestParam("announcement_id") int announcementId){
		return priceService.getSummaryStatisticsOfPeriod(announcementRep.findById(announcementId).get(), Period.ofDays(30));
	}
	@GetMapping("/announcement_info")
	public AnnouncementInfo getAnnouncementInfo(@RequestParam("announcement_id") int announcementId,@RequestParam("period") int days){
		return priceService.getAnnouncementInfo(announcementRep.findById(announcementId).get(), Period.ofDays(days));
	}
	
}
