package tn.dari.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Criteria;
import tn.dari.spring.service.interfaces.CriteriaService;

@RestController
@RequestMapping("/criteria")
public class CriteriaController {
	
	@Autowired
	CriteriaService criteriaService;
	
	@PostMapping("/add")
	public Criteria add(@RequestBody Criteria criteria, @RequestParam("user_id") int userId){
		criteriaService.add(criteria,userId);
		return criteria;
	}
	@GetMapping("/getall")
	public List<Criteria> getAll(){
		return criteriaService.fetchAll();
	}
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id")  int id){
		criteriaService.deleteById(id);
	}
	@PutMapping("/update")
	public Criteria update(@RequestBody Criteria criteria, @RequestParam("id") int id){
		criteriaService.update(criteria, id);
		return criteria;
	}
	
}
