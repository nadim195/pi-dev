package tn.dari.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Documents;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.ProfileRepository;
import tn.dari.spring.service.interfaces.DocumentsService;

@RestController
@RequestMapping("/documents")
public class DocumentsController {
	
	@Autowired
	DocumentsService documentsService;
	@Autowired
	AppUserRepository userRep;
	
	@PostMapping("/add")
	public Documents add(@RequestBody Documents documents, @RequestParam("user_id") int userId){
		documentsService.add(documents,userId);
		return documents;
	}
	@GetMapping("/getall")
	public List<Documents> getAll(){
		return documentsService.fetchAll();
	}
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id") int id){
		documentsService.deleteById(id);
	}
	@PutMapping("/update")
	public Documents update(@RequestBody Documents documents, @RequestParam("id") int id){
		documentsService.update(documents, id);
		return documents;
	}
	@GetMapping("/getOne")
	public Documents getByProfile(@RequestParam("user_id") int userId){
		return documentsService.fetchByUser(userRep.findById(userId).get());
	}
	
}
