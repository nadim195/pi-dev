package tn.dari.spring.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.Notification;
import tn.dari.spring.repository.NotificationRepository;
import tn.dari.spring.service.interfaces.FavouriteAnnoucementService;
import tn.dari.spring.service.interfaces.NotificationService;

@RestController
@RequestMapping("/notification")
public class NotificationController {
	
	@Autowired
	NotificationService notificationService;
	@Autowired
	FavouriteAnnoucementService favService;
	@Autowired
	NotificationRepository notifRep;
	
	@PostMapping("/add")
	public Notification add(@RequestBody Notification notification,@RequestParam("announcement_id") int announcementId,@RequestParam("user_id") int userId){		
		notificationService.add(notification,announcementId,userId);	
		return notification;
	}
	
	@GetMapping("/getall")
	public List<Notification> getAll(){
		return notificationService.fetchAll();
	}
	
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id")  int id){
		notificationService.deleteById(id);
	}

	@PutMapping("/update")
	public Notification update(@RequestBody Notification notification,@RequestParam("id") int id){		
		notificationService.update(notification, id);	
		return notification;
	}
	@GetMapping("/getunread")
	public List<Notification> getUnread (@RequestParam("user_id") int userId){
		return notificationService.fetchUnreadByUser(userId);
	}
	@PutMapping("/read")
	public Notification read(@RequestParam("id") int id){
		Notification notification = notifRep.findById(id).get();
		notificationService.read(notification);
		return notification;
	}
	
	
}
