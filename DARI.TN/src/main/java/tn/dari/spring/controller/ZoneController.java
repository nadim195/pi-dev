package tn.dari.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.ZonePrice;
import tn.dari.spring.service.interfaces.ZonePriceService;

@RestController
@RequestMapping("/zone")
public class ZoneController {
	
	@Autowired
	ZonePriceService zoneService;

	
	@PostMapping("/add")
	public ZonePrice add(@RequestBody ZonePrice zonePrice){
		zoneService.add(zonePrice);
		return zonePrice;
	}
	@GetMapping("/getall")
	public List<ZonePrice> getAll(){
		return zoneService.fetchAll();
	}
	@DeleteMapping("/delete")
	public void delete(@RequestParam("id") int id){
		zoneService.deleteById(id);
	}
	@PutMapping("/update")
	public ZonePrice update(@RequestBody ZonePrice zonePrice, @RequestParam("id") int id){
		zoneService.update(zonePrice, id);
		return zonePrice;
	}
	
}
