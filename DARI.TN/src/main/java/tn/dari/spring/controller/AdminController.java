package tn.dari.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Role;
import tn.dari.spring.service.interfaces.AnnouncementService;
import tn.dari.spring.service.interfaces.AppUserService;
import tn.dari.spring.service.interfaces.TestService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	TestService service;
	
	@Autowired
	AppUserService appUserService;

	@Autowired
	AnnouncementService annService;
	@PostMapping("/addRole")
	public Role addRole(@RequestBody Role r) {
		service.addRole(r);
		return r;
	}
	
	@PostMapping("/addAdmin")
	public AppUser addAdmin(@RequestBody AppUser appUser) {
		appUserService.addAdmin(appUser);
		return appUser;
	}
	@DeleteMapping("/delete")
	public void deleteUser(@RequestParam("id") int id) {
		appUserService.deleteUser(id);
	}
	@GetMapping("/users")
	public ArrayList<AppUser> getUsers() {
		return new ArrayList<AppUser>(appUserService.getUsers());
	}
	@GetMapping("/simpleUsers")
	public ArrayList<AppUser> getSimpleUsers() {
		return new ArrayList<AppUser>(appUserService.getSimpleUsers());

	}
	@GetMapping("/subscribedUsers")
	public ArrayList<AppUser> getSubscribedUsers() {
		return new ArrayList<AppUser>(appUserService.getSubscribedUsers());

	}
	@PostMapping("verifyAnnouncements")
	public void verifyAnnouncements(@RequestParam("id") int annId)
	{
		appUserService.validateAnnoucement(annId);
	}
	@GetMapping("PendingRequests")
	public List<Announcement> pendingAnnouncements()
	{
		return annService.loadNotVerifiedAnnouncements();
	}

}
