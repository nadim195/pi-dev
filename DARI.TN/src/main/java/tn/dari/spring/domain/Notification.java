package tn.dari.spring.domain;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Notification {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int notificationId;
	@Enumerated(EnumType.STRING)
	private NotificationType notifType;
	private boolean isRead;
	@ManyToOne
	private AppUser user;
	@ManyToOne
	private Announcement announcement;
	
	public int getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(int notifId) {
		this.notificationId = notifId;
	}
	public NotificationType getNotifType() {
		return notifType;
	}
	public void setNotifType(NotificationType notifType) {
		this.notifType = notifType;
	}
	public boolean isRead() {
		return isRead;
	}
	public void setIsRead(boolean isRead) {
		this.isRead = isRead;
	}
	
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}
	public Announcement getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(Announcement announcement) {
		this.announcement = announcement;
	}
	public Notification(NotificationType notifType, AppUser user, Announcement announcement) {
		super();
		this.notifType = notifType;
		this.user = user;
		this.announcement = announcement;
		isRead=false;
	}
	public Notification() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
