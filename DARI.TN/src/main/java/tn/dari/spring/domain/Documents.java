package tn.dari.spring.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Documents {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int documentId;
	private String payslip;
	private String idCard;
	private String engagementLetter;
	private String depositProof;
	@OneToOne
	private AppUser user;
	
	public int getDocumentId() {
		return documentId;
	}
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	public String getPayslip() {
		return payslip;
	}
	public void setPayslip(String payslip) {
		this.payslip = payslip;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public String getEngagementLetter() {
		return engagementLetter;
	}
	public void setEngagementLetter(String engagementLetter) {
		this.engagementLetter = engagementLetter;
	}
	public String getDepositProof() {
		return depositProof;
	}
	public void setDepositProof(String depositProof) {
		this.depositProof = depositProof;
	}
	public Documents(String payslip, String idCard, String engagementLetter, String depositProof) {
		super();
		this.payslip = payslip;
		this.idCard = idCard;
		this.engagementLetter = engagementLetter;
		this.depositProof = depositProof;
	}
	public Documents() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}

	
	
}
