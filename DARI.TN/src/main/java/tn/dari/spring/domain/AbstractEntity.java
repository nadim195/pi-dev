package tn.dari.spring.domain;

public interface AbstractEntity<E> {
    E getId();
}
