package tn.dari.spring.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Report {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int reportId;
	@ManyToOne
	private AppUser reportingUser;
	@ManyToOne
	private AppUser reportedUser;
	public Report() {
	}
	public int getReportId() {
		return reportId;
	}
	public void setReportId(int reportId) {
		this.reportId = reportId;
	}
	public AppUser getReportingUser() {
		return reportingUser;
	}
	public void setReportingUser(AppUser reportingUser) {
		this.reportingUser = reportingUser;
	}
	public AppUser getReportedUser() {
		return reportedUser;
	}
	public void setReportedUser(AppUser reportedUser) {
		this.reportedUser = reportedUser;
	}
	
	

}
