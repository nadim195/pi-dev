package tn.dari.spring.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
public class Role {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int roleId;
	private String roleName;
	@OneToMany(cascade = CascadeType.PERSIST,mappedBy="role",fetch=FetchType.LAZY)
	@JsonManagedReference
	private Set<AppUser> users;
	public Role() {
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Role(String roleName) {
		super();
		this.roleName = roleName;
	}
	public Set<AppUser> getUsers() {
		return users;
	}
	public void setUsers(Set<AppUser> users) {
		this.users = users;
	}
	public void addAll(Set<Role> roles) {
		// TODO Auto-generated method stub
		
	}
	
	
/// addAll.Set<Role>
//	
//	public void addAll(Set<Role> roles) {
//		// TODO Auto-generated method stub
//	
//	}
//	

	

}
