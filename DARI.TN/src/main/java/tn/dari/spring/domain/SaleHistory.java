package tn.dari.spring.domain;

import java.time.Instant;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class SaleHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Temporal(TemporalType.DATE)
	private Date saleDate;
	@OneToOne(mappedBy ="saleHistory")
	Announcement announcement;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}
	public Announcement getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(Announcement announcement) {
		this.announcement = announcement;
	}
	public SaleHistory(int id, Date saleDate, Announcement announcement) {
		super();
		this.id = id;
		this.saleDate = saleDate;
		this.announcement = announcement;
	}
	public SaleHistory(Announcement announcement) {
		super();
		saleDate=Date.from(Instant.now());
		this.announcement = announcement;
	}
	public SaleHistory() {
		super();
		saleDate=Date.from(Instant.now());
		// TODO Auto-generated constructor stub
	}
	
	
}
