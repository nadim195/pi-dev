package tn.dari.spring.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Reclamation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int reclamationId;
	private String reclamationStatus;
	private String description;
	@ManyToOne
	private AppUser user;
	public Reclamation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getReclamationStatus() {
		return reclamationStatus;
	}
	public void setReclamationStatus(String reclamationStatus) {
		this.reclamationStatus = reclamationStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}
	
	

}
