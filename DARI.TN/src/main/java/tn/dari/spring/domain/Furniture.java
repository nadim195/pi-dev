package tn.dari.spring.domain;

import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Furniture {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)

	private int id;

	private String furnitureName;
	private String fabricator;
	private String category;
	private double shippingPrice;
	private int numberOfPieces;
	private double FurniturePrice;
	private Boolean verified;
	private boolean active=true;
	
	

	@Column(columnDefinition="text")
	private String description;
	private int inStockNumber;
	
	
@ManyToOne
@JsonIgnore
private AppUser user;
//	@ManyToMany(mappedBy="favouritesFurniture")
//	@JsonIgnore
//	private Set<AppUser> users;
	
	
	
	@Transient
	private MultipartFile furnitureImage;
	
	@OneToMany(mappedBy = "furniture")
	
//	@JsonIgnore
//	private List<FurnitureToCartItem> furnitureToCartItemList;		
//	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFurnitureName() {
		return furnitureName;
	}

	public void setFurnitureName(String furnitureName) {
		this.furnitureName = furnitureName;
	}

	public String getFabricator() {
		return fabricator;
	}

	public void setFabricator(String fabricator) {
		this.fabricator = fabricator;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getShippingPrice() {
		return shippingPrice;
	}

	public void setShippingPrice(double shippingPrice) {
		this.shippingPrice = shippingPrice;
	}

	public int getNumberOfPieces() {
		return numberOfPieces;
	}

	public void setNumberOfPieces(int numberOfPieces) {
		this.numberOfPieces = numberOfPieces;
	}

	public double getFurniturePrice() {
		return FurniturePrice;
	}

	public void setFurniturePrice(double furniturePrice) {
		FurniturePrice = furniturePrice;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getInStockNumber() {
		return inStockNumber;
	}

	public void setInStockNumber(int inStockNumber) {
		this.inStockNumber = inStockNumber;
	}

	public MultipartFile getFurnitureImage() {
		return furnitureImage;
	}

	public void setFurnitureImage(MultipartFile furnitureImage) {
		this.furnitureImage = furnitureImage;
	}

//	public List<FurnitureToCartItem> getFurnitureToCartItemList() {
//		return furnitureToCartItemList;
//	}
//
//	public void setFurnitureToCartItemList(List<FurnitureToCartItem> furnitureToCartItemList) {
//		this.furnitureToCartItemList = furnitureToCartItemList;
//	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}



	public void setUser(AppUser user) {
		this.user = user;
	}

	public AppUser getUser() {
	return user;
}


//	public Set<AppUser> getUsers() {
//		return users;
//	}
//	public void setUsers(Set<AppUser> users) {
//		this.users = users;
//	}
}
