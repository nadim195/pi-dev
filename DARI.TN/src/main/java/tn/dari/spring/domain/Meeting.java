package tn.dari.spring.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Meeting {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int meetinId;
	@JsonFormat(pattern="dd-MM-YYYY")
	@Temporal(TemporalType.DATE)
	private Date meetingDate;
	private int fromHour;
	private int toHour;
	private boolean confirmed;
	@ManyToOne
	private AppUser client;
	@ManyToOne
	private AppUser user;
	public Meeting() {
	}
	public int getMeetinId() {
		return meetinId;
	}
	public void setMeetinId(int meetinId) {
		this.meetinId = meetinId;
	}
	public Date getMeetingDate() {
		return meetingDate;
	}
	public void setMeetingDate(Date meetingDate) {
		this.meetingDate = meetingDate;
	}
	public int getFromHour() {
		return fromHour;
	}
	public void setFromHour(int fromHour) {
		this.fromHour = fromHour;
	}
	public int getToHour() {
		return toHour;
	}
	public void setToHour(int toHour) {
		this.toHour = toHour;
	}
	public AppUser getClient() {
		return client;
	}
	public void setClient(AppUser client) {
		this.client = client;
	}
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}
	public boolean isConfirmed() {
		return confirmed;
	}
	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}
	
	

}
