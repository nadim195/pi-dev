package tn.dari.spring.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Credit implements AbstractEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Double loan;
    @Column(nullable = false)
    private Double monthlyPayment;
    @Column(nullable = false)
    private Long duration;
    @Column(nullable = false)
    private Double salary;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date createdOn = new Date();

    @ManyToOne(optional = false)
    private AppUser user;

    @ManyToOne(optional = false)
    private BankSpecification bankSpecification;

    @Override
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
