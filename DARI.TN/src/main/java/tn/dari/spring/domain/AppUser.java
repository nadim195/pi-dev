package tn.dari.spring.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//import tn.dari.spring.domain.Order;
//import tn.dari.spring.domain.ShoppingCart;
import tn.dari.spring.domain.UserPayment;
//import tn.dari.spring.domain.UserShipping;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity(name = "User")
public class AppUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	@JsonFormat(pattern="dd-MM-YYYY")
	@Temporal(TemporalType.DATE)
	private Date createdAt;
	@JsonFormat(pattern="dd-MM-YYYY")
	@Temporal(TemporalType.DATE)
	private Date updatedAt;
	private String adress;
	private int phoneNumber;
	private boolean subscribed;
	private String confirmation_token;
	private String idStrype;
	private boolean verified;
	private int reportsNumber ; 
	@OneToMany(mappedBy = "user")
	@JsonIgnore
	private Set<Subscription> subscriptions;
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JsonBackReference
	private Role role;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	@JsonIgnore
	private Set<Reclamation> reclamations;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	@JsonIgnore
	private Set<Announcement> announcements;
	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "Favourite")
	private Set<Announcement> favouritesAnnouncements;
	@JsonIgnore
	@ManyToMany
	@JoinTable(name = "History")
	private Set<Announcement> AnnouncementsHistory;
	@OneToOne(cascade = CascadeType.ALL)
	@JsonIgnore
	private Profile profile;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy="user")
	private Set<Availability> availibility; 
	@JsonIgnore
	@OneToMany
	private Set<Meeting> meetings; 
	@JsonIgnore
	@OneToMany
	private Set<Report> reports; 
	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private Documents documents;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	@JsonIgnore
	private Set<Interest> interests;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	@JsonIgnore
	private Set<Notification> notifications;
	@OneToMany(cascade = CascadeType.ALL,mappedBy= "user")
	@JsonIgnore
	private Set<Criteria> criterias;

	
	/*
	// FURNITURE SHOPPING CART 
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	private ShoppingCart shoppingCart;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private List<UserShipping> userShippingList;
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	private List<UserPayment> userPaymentList;
	
	@OneToMany(mappedBy = "user")
	private List<Order> orderList;
	
	
	public List<UserShipping> getUserShippingList() {
		return userShippingList;
	}

	public void setUserShippingList(List<UserShipping> userShippingList) {
		this.userShippingList = userShippingList;
	}

	public List<UserPayment> getUserPaymentList() {
		return userPaymentList;
	}

	public void setUserPaymentList(List<UserPayment> userPaymentList) {
		this.userPaymentList = userPaymentList;
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	
	
	*/
	
	
	
	
	
	
	
	//////////////////////////////////////
	
	

	public Set<Announcement> getFavouritesAnnouncements() {
		return favouritesAnnouncements;
	}

	public void setFavouritesAnnouncements(Set<Announcement> favouritesAnnouncements) {
		this.favouritesAnnouncements = favouritesAnnouncements;
	}

	public Set<Announcement> getAnnouncementsHistory() {
		return AnnouncementsHistory;
	}

	public void setAnnouncementsHistory(Set<Announcement> announcementsHistory) {
		AnnouncementsHistory = announcementsHistory;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public AppUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getIdStrype() {
		return idStrype;
	}

	public void setIdStrype(String idStrype) {
		this.idStrype = idStrype;
	}

	public String getConfirmation_token() {
		return confirmation_token;
	}

	public void setConfirmation_token(String confirmation_token) {
		this.confirmation_token = confirmation_token;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isSubscribed() {
		return subscribed;
	}

	public void setSubscribed(boolean subscribed) {
		this.subscribed = subscribed;
	}

	
	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public Set<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(Set<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Set<Reclamation> getReclamations() {
		return reclamations;
	}

	public void setReclamations(Set<Reclamation> reclamations) {
		this.reclamations = reclamations;
	}

	public Set<Announcement> getAnnouncements() {
		return announcements;
	}

	public void setAnnouncements(Set<Announcement> announcements) {
		this.announcements = announcements;
	}

	public Set<Criteria> getCriterias() {
		return criterias;
	}

	public void setCriterias(Set<Criteria> criterias) {
		this.criterias = criterias;
	}

	public AppUser(String firstName, String lastName, String email, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	 public Set<Availability> getAvailibility() {
		return availibility;
	}

	public void setAvailibility(Set<Availability> availibility) {
		this.availibility = availibility;
	}

	public int getReportsNumber() {
		return reportsNumber;
	}

	public void setReportsNumber(int signal) {
		this.reportsNumber = signal;
	}

	public Set<Meeting> getMeetings() {
		return meetings;
	}

	public void setMeetings(Set<Meeting> meetings) {
		this.meetings = meetings;
	}

	public Set<Report> getReports() {
		return reports;
	}

	public void setReports(Set<Report> reports) {
		this.reports = reports;
	}

	public Documents getDocuments() {
		return documents;
	}

	public void setDocuments(Documents documents) {
		this.documents = documents;
	}

	public Set<Interest> getInterests() {
		return interests;
	}

	public void setInterests(Set<Interest> interests) {
		this.interests = interests;
	}

	public Set<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	} 
	
	

}
