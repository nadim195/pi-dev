package tn.dari.spring.domain;

import lombok.Data;
import tn.dari.spring.dto.LoanPreferences;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
public class BankSpecification implements AbstractEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String bankName;

    @Enumerated(EnumType.ORDINAL)
    private LoanPreferences preferences;
    private Long value;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankSpecification that = (BankSpecification) o;
        return Objects.equals(id, that.id) && bankName.equals(that.bankName) && preferences == that.preferences && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bankName, preferences);
    }
}
