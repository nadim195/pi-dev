package tn.dari.spring.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Criteria {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int CriteriaId;
	@Enumerated(EnumType.STRING)
	private AnnouncementType announcementType;
	private String criteriaName;
	private String estateType;
	private String location;
	private double minPrice;
	private double maxPrice;
	private double minSurface;
	private double maxSurface;
	private int minNumberOfFloors;
	private int maxNumberOfFloors;
	private int minNumberOfRooms;
	private int maxNumberOfRooms;
	@ManyToOne
	//@JsonIgnore
	private AppUser user;
	
	public int getCriteriaId() {
		return CriteriaId;
	}
	public void setCriteriaId(int criteriaId) {
		CriteriaId = criteriaId;
	}
	public AnnouncementType getAnnouncementType() {
		return announcementType;
	}
	public void setAnnouncementType(AnnouncementType announcementType) {
		this.announcementType = announcementType;
	}
	public String getEstateType() {
		return estateType;
	}
	public void setEstateType(String estateType) {
		this.estateType = estateType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}
	public double getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}
	public double getMinSurface() {
		return minSurface;
	}
	public void setMinSurface(double minSurface) {
		this.minSurface = minSurface;
	}
	public double getMaxSurface() {
		return maxSurface;
	}
	public void setMaxSurface(double maxSurface) {
		this.maxSurface = maxSurface;
	}
	public int getMinNumberOfFloors() {
		return minNumberOfFloors;
	}
	public void setMinNumberOfFloors(int minNumberOfFloors) {
		this.minNumberOfFloors = minNumberOfFloors;
	}
	public int getMaxNumberOfFloors() {
		return maxNumberOfFloors;
	}
	public void setMaxNumberOfFloors(int maxNumberOfFloors) {
		this.maxNumberOfFloors = maxNumberOfFloors;
	}
	public int getMinNumberOfRooms() {
		return minNumberOfRooms;
	}
	public void setMinNumberOfRooms(int minNumberOfRooms) {
		this.minNumberOfRooms = minNumberOfRooms;
	}
	public int getMaxNumberOfRooms() {
		return maxNumberOfRooms;
	}
	public void setMaxNumberOfRooms(int maxNumberOfRooms) {
		this.maxNumberOfRooms = maxNumberOfRooms;
	}
	public Criteria(int criteriaId, AnnouncementType announcementType, String criteriaName, String estateType, String location,
			double minPrice, double maxPrice, double minSurface, double maxSurface, int minNumberOfFloors,
			int maxNumberOfFloors, int minNumberOfRooms, int maxNumberOfRooms) {
		super();
		CriteriaId = criteriaId;
		this.announcementType = announcementType;
		this.criteriaName = criteriaName;
		this.estateType = estateType;
		this.location = location;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
		this.minSurface = minSurface;
		this.maxSurface = maxSurface;
		this.minNumberOfFloors = minNumberOfFloors;
		this.maxNumberOfFloors = maxNumberOfFloors;
		this.minNumberOfRooms = minNumberOfRooms;
		this.maxNumberOfRooms = maxNumberOfRooms;
	}
	public Criteria() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getCriteriaName() {
		return criteriaName;
	}
	public void setCriteriaName(String criteriaName) {
		this.criteriaName = criteriaName;
	}
	public AppUser getUser() {
		return user;
	}
	public void setUser(AppUser user) {
		this.user = user;
	}

	
	
	
	

}
