package tn.dari.spring.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Availability {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@JsonFormat(pattern="dd-MM-YYYY")
	@Temporal(TemporalType.DATE)
	private Date date;
	private int fromHour;
	private int toHour;
	@ManyToOne
	private AppUser user;
	public Availability() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getFromHour() {
		return fromHour;
	}

	public void setFromHour(int from) {
		this.fromHour = from;
	}

	public int getToHour() {
		return toHour;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public void setToHour(int toHour) {
		this.toHour = toHour;
	}
	

	

}
