package tn.dari.spring.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ZonePrice {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int zoneId;
	private double pricem2;
	private String zone;
	
	public int getZoneId() {
		return zoneId;
	}
	public void setZoneId(int zoneId) {
		this.zoneId = zoneId;
	}
	public double getPricem2() {
		return pricem2;
	}
	public void setPricem2(double pricem2) {
		this.pricem2 = pricem2;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public ZonePrice(int zoneId, double pricem2, String zone) {
		super();
		this.zoneId = zoneId;
		this.pricem2 = pricem2;
		this.zone = zone;
	}
	public ZonePrice() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
