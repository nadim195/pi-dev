package tn.dari.spring.domain;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tn.dari.spring.repository.ProductRepository;


import java.util.List;

@SuppressWarnings("deprecation")
@Repository
@Transactional
public class Product implements ProductRepository {


    @Autowired
    private SessionFactory sessionFactory;

    public Product getProductById (int id){
        Session session = sessionFactory.getCurrentSession();
        Product product = (Product) session.get(Product.class, id);
        session.flush();

        return product;
    }

    public List<Product> getProductList(){
        Session session = sessionFactory.getCurrentSession();
        @SuppressWarnings("rawtypes")
		Query query = session.createQuery("from Product");
        @SuppressWarnings("unchecked")
		List<Product> productList = query.list();
        session.flush();
// session createQuery in here , ne9sa !
        return productList;
    }

    public void addProduct (Product product){
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }

    public void editProduct (Product product){
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }

    public void deleteProduct (Product product){
        Session session = sessionFactory.getCurrentSession();
        session.delete(product);
        session.flush();
    }

} 


