package tn.dari.spring.domain;

import java.time.Instant;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class RentHistory { 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Temporal(TemporalType.DATE)
	private Date rentDate;
	@OneToOne(mappedBy = "rentHistory")
	Announcement announcement;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getRentDate() {
		return rentDate;
	}
	public void setRentDate(Date rentDate) {
		this.rentDate = rentDate;
	}
	public Announcement getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(Announcement announcement) {
		this.announcement = announcement;
	}
	public RentHistory(int id, Date rentDate, Announcement announcement) {
		super();
		this.id = id;
		this.rentDate = rentDate;
		this.announcement = announcement;
	}
	public RentHistory(Announcement announcement) {
		super();
		rentDate=Date.from(Instant.now());
		this.announcement = announcement;
	}
	public RentHistory() {
		super();
		rentDate=Date.from(Instant.now());
		// TODO Auto-generated constructor stub
	}
	
	
}