package tn.dari.spring.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.Period;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Announcement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int announcementId;
	@Enumerated(EnumType.STRING)
	private AnnouncementType type;
	private String estateType;
	private String location;
	private double price;
	private String description;
	private double surface;
	private int numberOfFloors;
	private int numberOfRooms;
	@Temporal(TemporalType.DATE)
	private Date dateCreated;
	@Temporal(TemporalType.DATE)
	private Date datePublished;
	private Boolean verified;
	private Boolean archived;
	@ManyToOne
	@JsonIgnore
	private AppUser user;
	@ManyToMany(mappedBy="favouritesAnnouncements")
	@JsonIgnore
	private Set<AppUser> users;
	@ManyToMany(mappedBy="AnnouncementsHistory")
	@JsonIgnore
	private Set<AppUser> userHistory;
	@OneToOne
	@JsonIgnore
	private SaleHistory saleHistory;
	@OneToOne
	@JsonIgnore
	private RentHistory rentHistory;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "announcement")
	@JsonIgnore
	private Set<PriceHistory> priceHistories;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "announcement")
	@JsonIgnore
	private Set<Interest> interests;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "announcement")
	@JsonIgnore
	private Set<Notification> notifications;
	
	
	public SaleHistory getSaleHistory() {
		return saleHistory;
	}
	public void setSaleHistory(SaleHistory saleHistory) {
		this.saleHistory = saleHistory;
	}
	public RentHistory getRentHistory() {
		return rentHistory;
	}
	public void setRentHistory(RentHistory rentHistory) {
		this.rentHistory = rentHistory;
	}
	public Set<PriceHistory> getPriceHistories() {
		return priceHistories;
	}
	public void setPriceHistories(Set<PriceHistory> priceHistories) {
		this.priceHistories = priceHistories;
	}
	public Set<Interest> getInterests() {
		return interests;
	}
	public void setInterests(Set<Interest> interests) {
		this.interests = interests;
	}
	public void setAnnouncementId(int announcementId) {
		this.announcementId = announcementId;
	}

	public AnnouncementType getType() {
		return type;
	}

	public void setType(AnnouncementType type) {
		this.type = type;
	}

	public String getEstateType() {
		return estateType;
	}

	public int getAnnouncementId() {
		return announcementId;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDatePublished() {
		return datePublished;
	}

	public void setDatePublished(Date datePublished) {
		this.datePublished = datePublished;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public void setEstateType(String estateType) {
		this.estateType = estateType;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String localtion) {
		this.location = localtion;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getSurface() {
		return surface;
	}

	public void setSurface(double surface) {
		this.surface = surface;
	}

	public int getNumberOfRooms() {
		return numberOfRooms;
	}

	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	public AppUser getUser() {
		return user;
	}

	public void setUser(AppUser user) {
		this.user = user;
	}

	public Announcement() {
		super();
	}

	public int getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(int numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}
	public Set<AppUser> getUsers() {
		return users;
	}
	public void setUsers(Set<AppUser> users) {
		this.users = users;
	}
	public Set<AppUser> getUserHistory() {
		return userHistory;
	}
	public void setUserHistory(Set<AppUser> userHistory) {
		this.userHistory = userHistory;
	}
	public Set<Notification> getNotifications() {
		return notifications;
	}
	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}
	public Boolean getArchived() {
		return archived;
	}
	public void setArchived(Boolean archived) {
		this.archived = archived;
	}
	public Announcement(AnnouncementType type, String estateType, String location, double price, String description,
			double surface, int numberOfFloors, int numberOfRooms) {
		super();
		this.type = type;
		this.estateType = estateType;
		this.location = location;
		this.price = price;
		this.description = description;
		this.surface = surface;
		this.numberOfFloors = numberOfFloors;
		this.numberOfRooms = numberOfRooms;
		this.dateCreated = Date.from(Instant.now().minus(Period.ofDays(30)));
		this.datePublished = Date.from(Instant.now().minus(Period.ofDays(29)));
		this.verified = true;
		this.archived = false;
	}
	

}
