package tn.dari.spring.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class SimulatePayload {

    @NonNull
    private Double salary;
    @NonNull
    private Double loan;

    private Float rate;
    private Long duration;

}
