package tn.dari.spring.dto;

import java.util.HashMap;
import java.util.Map;

public class CreditDTO extends AbstractDTO<Long> {
    private Long id;
    private Double loan;
    private Double monthlyPayment;
    private Long duration;
    private String user;
    private String bankName;

    private Map<Integer, Double> repayments = new HashMap<>();

    public CreditDTO() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    public void setLoan(Double loan) {
        this.loan = loan;
    }

    public Double getLoan() {
        return this.loan;
    }

    public void setMonthlyPayment(Double monthlyPayment) {
        this.monthlyPayment = monthlyPayment;
    }

    public Double getMonthlyPayment() {
        return this.monthlyPayment;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getDuration() {
        return this.duration;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return this.user;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Map<Integer, Double> getRepayments() {
        return repayments;
    }

    public void setRepayments(Map<Integer, Double> repayments) {
        this.repayments = repayments;
    }
}
