package tn.dari.spring.dto;

import lombok.Getter;

@Getter
public enum LoanPreferences {
    TMM(1, "tmm"),
    INTEREST_RATE(2, "interest-rate"),
    DURATION(3, "duration");

    private final int id;
    private final String criteria;


    LoanPreferences(int id, String criteria) {
        this.id = id;
        this.criteria = criteria;
    }
}
