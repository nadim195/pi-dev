package tn.dari.spring.repository;

import java.util.List;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Notification;

public interface NotificationRepository extends CrudRepository<Notification, Integer>{
	public List<Notification> findByUser(AppUser user);
	public List<Notification> findByIsReadFalse();
}
