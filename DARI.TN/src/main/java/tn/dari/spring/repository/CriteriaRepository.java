package tn.dari.spring.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Criteria;

public interface CriteriaRepository extends CrudRepository<Criteria, Integer>{

	public List<Criteria> findByUser (AppUser user);
	public Criteria findByCriteriaName (String name);
}
