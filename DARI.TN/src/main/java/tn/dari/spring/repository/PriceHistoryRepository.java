package tn.dari.spring.repository;

import java.util.Date;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.PriceHistory;

public interface PriceHistoryRepository extends CrudRepository<PriceHistory, Integer>{
	public PriceHistory findDistinctByAnnouncement(Announcement announcement);
	public Set<PriceHistory> findByAnnouncement(Announcement announcement);
	public Set<PriceHistory> findByUpdateDateAfter(Date date);
}
