package tn.dari.spring.repository;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.RentHistory;

public interface RentHistoryRepository extends CrudRepository<RentHistory, Integer>{
	
	//public RentHistory findByAnnouncement(Announcement announcementId);
	//public List<RentHistory> findByClient(AppUser client);

}
