package tn.dari.spring.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import tn.dari.spring.domain.Credit;

@Repository
public interface CreditRepository extends PagingAndSortingRepository<Credit, Long> {
}
