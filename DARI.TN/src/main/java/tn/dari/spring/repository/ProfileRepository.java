package tn.dari.spring.repository;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.Profile;

public interface ProfileRepository extends CrudRepository<Profile, Integer> {
	
}
