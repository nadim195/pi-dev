package tn.dari.spring.repository;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Meeting;

public interface MeetingRepository extends CrudRepository<Meeting, Integer>{
		public Meeting findByClientAndUser(AppUser client,AppUser user);
}
