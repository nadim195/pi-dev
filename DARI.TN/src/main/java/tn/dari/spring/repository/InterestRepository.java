package tn.dari.spring.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Interest;

public interface InterestRepository extends CrudRepository<Interest, Integer>{
	public List<Interest> findByAcceptedTrue();
	public Interest findByUserAndAnnouncement(AppUser user, Announcement announcement);

}
