package tn.dari.spring.repository;

import tn.dari.spring.domain.BankSpecification;
import org.springframework.data.jpa.repository.JpaRepository;
import tn.dari.spring.dto.LoanPreferences;

import java.util.List;

public interface BankSpecificationRepository extends JpaRepository<BankSpecification, Long> {

    List<BankSpecification> findAllByPreferencesAndValueBetweenOrderByValue(LoanPreferences preferences, Long value1, Long value2);

}
