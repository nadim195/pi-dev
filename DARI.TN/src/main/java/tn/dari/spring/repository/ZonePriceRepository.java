package tn.dari.spring.repository;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.ZonePrice;

public interface ZonePriceRepository extends CrudRepository<ZonePrice, Integer>{

}
