package tn.dari.spring.repository;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.SaleHistory;

public interface SaleHistoryRepository extends CrudRepository<SaleHistory, Integer>{
	
	//public SaleHistory findByAnnouncement(Announcement announcementId);
	//public List<SaleHistory> findByClient(AppUser client);

}
