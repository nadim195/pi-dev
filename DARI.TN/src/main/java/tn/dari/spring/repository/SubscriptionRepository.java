package tn.dari.spring.repository;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Subscription;

public interface SubscriptionRepository extends CrudRepository<Subscription, Integer>{
	//public Subscription findByUserId(int userid); 
	public Subscription findBySubscriptionId(int id);

}
