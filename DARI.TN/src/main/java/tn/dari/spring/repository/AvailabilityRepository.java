package tn.dari.spring.repository;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Availability;

public interface AvailabilityRepository extends CrudRepository<Availability,Integer>{

	// @Query("select a from Availability a where a.date=:date and a.user=:user")
	//public Availability findByDateAndUser(@Param("date") Date date,@Param("user") AppUser user);
	Availability findByDateAndUser(Date date,AppUser user);
}
