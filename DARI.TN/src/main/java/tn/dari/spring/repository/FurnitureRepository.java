package tn.dari.spring.repository;
 


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.Furniture;

public interface FurnitureRepository extends CrudRepository<Furniture, Long>{


	public List<Furniture> findByCategory(String category);
	public List<Furniture> findByFurnitureName(String FurnitureName);
	//public List<Furniture> findByFurniturePrice(double FurniturePrice);
	//public List<Furniture> findByid(int id);
	public Furniture findByid(int id);
	public List<Furniture> findByInStockNumber(int inStockNumber);
	public List<Furniture> findByNumberOfPieces(int numberOfPieces);
	public List<Furniture> findByShippingPrice(double shippingPrice);
	public List<Furniture> findByActiveTrue();
	public List<Furniture> findByActiveFalse();
	public List<Furniture> findByVerifiedTrue();
	public List<Furniture> findByVerifiedFalse();
	public void deleteById(int id);


}
