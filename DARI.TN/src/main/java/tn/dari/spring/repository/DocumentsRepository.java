package tn.dari.spring.repository;
import org.springframework.data.repository.CrudRepository;

import tn.dari.spring.domain.Documents;


public interface DocumentsRepository extends CrudRepository<Documents, Integer>{

}
