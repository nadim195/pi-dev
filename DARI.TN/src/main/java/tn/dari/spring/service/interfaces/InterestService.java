package tn.dari.spring.service.interfaces;

import java.util.List;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.Interest;
import tn.dari.spring.domain.util.InterestDocuments;

public interface InterestService {
	
	public Interest add (Interest interest, int userId, int announcementId);
	public Interest update(Interest interest, int id);
	public int delete(Interest interest);
	public int deleteById(int id);
	public int declare(int announcementId, int userId);
	public int accept(int id);
	public List<Interest> fetchAll();
	public List<Interest> fetchAllByAnnouncement(Announcement announcement);
	public List<Interest> fetchAllByAnnouncementId(int announcementId);
	public List<Interest> fetchAllAcceptedByAnnouncement(int announcementId);
	public List<Interest> fetchAllAcceptedByPoster(int userId);
	public List<Interest> fetchAllByPoster(int userId);
	public void reject(int id);
	public InterestDocuments getInterestDocuments(Interest interest);

}
