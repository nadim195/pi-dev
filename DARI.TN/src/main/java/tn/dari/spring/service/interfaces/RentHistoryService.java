package tn.dari.spring.service.interfaces;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.RentHistory;
import java.util.List;

public interface RentHistoryService {

	public RentHistory add(RentHistory rentHistory, int announcementId);
	public RentHistory update(RentHistory rentHistory, int id);
	public RentHistory markRented(Announcement announcement);
	public RentHistory markRented(int announcementId);
	public RentHistory fetchOne(int id);
	public int delete(RentHistory rentHistory);
	public int deleteById(int rentHistoryId);
	public List<RentHistory> fetchAll();
	

}
