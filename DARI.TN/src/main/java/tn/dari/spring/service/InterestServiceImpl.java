package tn.dari.spring.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AnnouncementType;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Interest;
import tn.dari.spring.domain.util.InterestDocuments;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.InterestRepository;
import tn.dari.spring.service.interfaces.InterestService;

@Service
public class InterestServiceImpl implements InterestService {
	
	@Autowired
	InterestRepository interestRep;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	AppUserRepository userRep;

	@Override
	public Interest add(Interest interest, int userId, int announcementId) {
		interest.setUser(userRep.findById(userId).get());
		interest.setAnnouncement(annRep.findById(announcementId).get());
		interestRep.save(interest);
		return interest;
	}

	@Override
	public int delete(Interest interest) {
		interestRep.delete(interest);
		return 1;
	}

	@Override
	public int deleteById(int id) {
		interestRep.deleteById(id);
		return 1;
	}

	@Override
	public Interest update(Interest interest, int id) {
		interest.setAnnouncement(interestRep.findById(id).get().getAnnouncement());
		interest.setUser(interestRep.findById(id).get().getUser());
		interest.setId(id);
		interestRep.save(interest);
		return interest;
	}

	@Override
	public List<Interest> fetchAll() {
		return (List<Interest>) interestRep.findAll();
	}
	
	@Override
	public List<Interest> fetchAllAcceptedByAnnouncement(int announcementId) {
		Announcement announcement = annRep.findById(announcementId).get();
		List<Interest> interests = announcement.getInterests().stream().collect(Collectors.toList());
		interests.removeIf(i -> !i.isAccepted());
		return interests;
	}

	@Override
	public List<Interest> fetchAllByAnnouncement(Announcement announcement) {
		return announcement.getInterests().stream().collect(Collectors.toList());
	}
	
	@Override
	public List<Interest> fetchAllByAnnouncementId(int announcementId) {
		return annRep.findById(announcementId).get().getInterests().stream().collect(Collectors.toList());
	}

	@Override
	public int declare(int announcementId, int userId) {
		Announcement announcement = annRep.findById(announcementId).get();
		if((announcement.getType().equals(AnnouncementType.HolidayRent))||(announcement.getType().equals(AnnouncementType.Rent))){
			AppUser user = userRep.findById(userId).get();
			Interest interest = interestRep.findByUserAndAnnouncement(user, announcement);
			if(Objects.isNull(interest))
				interest = new Interest();
			else
				return 0;
			interest.setUser(user);
			interest.setAnnouncement(announcement);
			interestRep.save(interest);
			return interest.getId();
			}
		return 0;
	}
	
	@Override
	public int accept(int id){
		Interest interest = interestRep.findById(id).get();
		interest.setAccepted(true);
		interestRep.save(interest);
		return 1;
	}
	
	@Override
	public void reject(int id){
		interestRep.deleteById(id);
	}

	@Override
	public List<Interest> fetchAllAcceptedByPoster(int userId) {
		return fetchAllByPoster(userId).stream().filter(i -> i.isAccepted()).collect(Collectors.toList());
	}
	
	@Override
	public List<Interest> fetchAllByPoster(int userId) {
		return userRep.findByUserId(userId).getAnnouncements().stream().flatMap(a -> a.getInterests().stream()).collect(Collectors.toList());
	}
	
	@Override
	public InterestDocuments getInterestDocuments (Interest interest){
		return new InterestDocuments(interest.getId(), interest.getDate(), interest.isAccepted(), interest.getUser().getEmail(), interest.getUser().getDocuments().getPayslip(), interest.getUser().getDocuments().getIdCard(), interest.getUser().getDocuments().getEngagementLetter(), interest.getUser().getDocuments().getDepositProof());
	}

}
