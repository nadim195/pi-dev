package tn.dari.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.Notification;
import tn.dari.spring.domain.NotificationType;
import tn.dari.spring.domain.RentHistory;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.RentHistoryRepository;
import tn.dari.spring.service.interfaces.NotificationService;
import tn.dari.spring.service.interfaces.RentHistoryService;

@Service
public class RentHistoryServiceImpl implements RentHistoryService {
	
	@Autowired
	RentHistoryRepository rentHistoryRep;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	NotificationService notifService;

	
	@Override
	public RentHistory markRented(int announcementId) {
		Announcement announcement = annRep.findById(announcementId).get();
		announcement.setArchived(true);
		annRep.save(announcement);
		RentHistory rentHistory = new RentHistory(announcement);
		rentHistoryRep.save(rentHistory);
		notifService.create(NotificationType.FavoriteRented, announcementId);
		return rentHistory;
	}
	
	@Override
	public RentHistory markRented(Announcement announcement) {
		announcement.setArchived(true);
		annRep.save(announcement);
		RentHistory rentHistory = new RentHistory(announcement);
		rentHistoryRep.save(rentHistory);
		notifService.create(NotificationType.FavoriteRented, announcement.getAnnouncementId());
		return rentHistory;
	}

	
	@Override
	public RentHistory add(RentHistory rentHistory, int announcementId) {
		rentHistory.setAnnouncement(annRep.findById(announcementId).get());
		rentHistoryRep.save(rentHistory);
		notifService.create(NotificationType.FavoriteRented, announcementId);
		return rentHistory;
	}

	@Override
	public int delete(RentHistory rentHistory) {
		rentHistoryRep.delete(rentHistory);
		return 1;
	}

	@Override
	public int deleteById(int id) {
		rentHistoryRep.deleteById(id);
		return 1;
	}

	@Override
	public RentHistory update(RentHistory rentHistory, int id) {
		rentHistory.setAnnouncement(rentHistoryRep.findById(id).get().getAnnouncement());
		rentHistory.setId(id);
		rentHistoryRep.save(rentHistory);
		return rentHistory;
	}

	@Override
	public List<RentHistory> fetchAll() {
		return (List<RentHistory>) rentHistoryRep.findAll();
	}

	@Override
	public RentHistory fetchOne(int id) {
		return rentHistoryRep.findById(id).get();
	}
	

}
