package tn.dari.spring.service.impl;

import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import tn.dari.spring.domain.Credit;
import tn.dari.spring.dto.CreditDTO;
import tn.dari.spring.mapper.CreditMapper;
import tn.dari.spring.repository.CreditRepository;
import tn.dari.spring.service.CreditService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CreditServiceImpl implements CreditService {
    private final CreditMapper mapper = Mappers.getMapper(CreditMapper.class);
    private final CreditRepository repository;

    public CreditServiceImpl(CreditRepository repository) {
        this.repository = repository;
    }

    @Override
    public CreditDTO save(Credit entity) {
        return mapper.asDTO(repository.save(entity));
    }

    @Override
    public List<CreditDTO> save(List<Credit> credits) {
        repository.saveAll(credits);
        return mapper.asDTOList(credits);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<CreditDTO> findById(Long id) {
        Optional<Credit> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.asDTO(entity))).orElse(null);
    }

    @Override
    public List<CreditDTO> findAll() {
        return mapper.asDTOList((List<Credit>) repository.findAll());
    }

    @Override
    public Page<CreditDTO> findAll(Pageable pageable) {
        Page<Credit> entityPage = repository.findAll(pageable);
        List<CreditDTO> dtos = mapper.asDTOList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public CreditDTO update(Credit entity, Long id) {
        Optional<CreditDTO> optionalDto = findById(entity.getId());
        if (optionalDto.isPresent()) {
            return save(entity);
        }
        return null;
    }
}
