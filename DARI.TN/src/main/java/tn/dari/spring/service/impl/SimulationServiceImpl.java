package tn.dari.spring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.BankSpecification;
import tn.dari.spring.domain.Credit;
import tn.dari.spring.dto.CreditDTO;
import tn.dari.spring.dto.LoanPreferences;
import tn.dari.spring.dto.SimulatePayload;
import tn.dari.spring.service.BankSpecificationService;
import tn.dari.spring.service.CreditService;
import tn.dari.spring.service.SimulationService;
import tn.dari.spring.service.interfaces.AppUserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimulationServiceImpl implements SimulationService {

    @Autowired
    private CreditService creditService;

    @Autowired
    private BankSpecificationService bankSpecificationService;

    @Autowired
    private AppUserService userDetailsService;

    @Value("${dari.rate-credit:10}")
    private float rateCredit;

    @Override
    public List<CreditDTO> simulate(SimulatePayload payload) {
        List<CreditDTO> result = new ArrayList<>();

        if (payload.getDuration() < 24 && payload.getDuration() > 30 * 12) {
            throw new UnsupportedOperationException("Duration must be > 2 years and < 30 years");
        }

        if (payload.getLoan() < 80000) {
            throw new UnsupportedOperationException("Loan must be > 80 000 TND");
        }

        if (payload.getRate() < 15 && payload.getRate() > 20) {
            throw new UnsupportedOperationException("Rate must between 15 and 25");
        }

        double loanDuration = (payload.getLoan() * (1 + (payload.getRate() / 100))) / payload.getDuration();
        if (loanDuration > payload.getSalary() * 0.4) {
            throw new UnsupportedOperationException("Salary must be > 40% of loan/duration");
        }

        //simulate
        List<BankSpecification> bankSpecByDuration = bankSpecificationService.findByValues(LoanPreferences.DURATION, payload.getDuration(), 30L * 12);
        for (BankSpecification bankSpecification : bankSpecByDuration) {
            Credit credit = new Credit();
            credit.setDuration(bankSpecification.getValue());
            credit.setMonthlyPayment(loanDuration);
            credit.setSalary(payload.getSalary());
            credit.setUser(userDetailsService.loadUserByEmail((String) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
            credit.setLoan(payload.getLoan());
            credit.setBankSpecification(bankSpecification);

            //POJO
            CreditDTO creditDTO = creditService.save(credit);
            float years = payload.getDuration().floatValue() / 12;
            for (int year = 0; year < (int) years; year++) {
                creditDTO.getRepayments().put(year + 1, credit.getMonthlyPayment() * 12);
            }
            if (payload.getDuration() % 12 != 0) {
                creditDTO.getRepayments().put((int) years + 1, credit.getMonthlyPayment() * (payload.getDuration() % 12));
            }

            result.add(creditDTO);
        }

        return result;
    }
}
