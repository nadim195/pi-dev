package tn.dari.spring.service.interfaces;

import tn.dari.spring.domain.Subscription;
import java.util.List;



public interface SubscriptionService {
	
	public void ActivateSubscription(Subscription sub, int userid);
	public List<Subscription> getAllSubscriptions();
	public Subscription getSubscriptionById(int id);
	public Subscription updateSubscription(Subscription s , int id);
	public List<Subscription> filterSub(String type);
	public Subscription LoadByUserId(int userid);

}
