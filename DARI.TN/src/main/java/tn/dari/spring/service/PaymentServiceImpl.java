package tn.dari.spring.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.StripeException;

import com.stripe.Stripe;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Order;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.exception.RateLimitException;
import com.stripe.exception.StripeException;

import com.stripe.Stripe;
import com.stripe.model.Charge;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Subscription;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.SubscriptionRepository;
import tn.dari.spring.service.interfaces.PaymentService;

@Service("paymentService")
public class PaymentServiceImpl implements PaymentService {
	@Autowired
	AppUserRepository userrep;
	@Autowired
	private SubscriptionRepository subrepo;

  private static final String TEST_STRIPE_SECRET_KEY = "sk_test_5xcidfSM2RvPXFD4xLaeC4Eu00YOAYlMo0";

  public PaymentServiceImpl() {
    Stripe.apiKey = TEST_STRIPE_SECRET_KEY;
  }

  public String createCustomer(AppUser user) {
	
    Map<String, Object> customerParams = new HashMap<String, Object>();
    customerParams.put("description", 
      user.getFirstName() + " " + user.getLastName());
	customerParams.put("email", user.getEmail());
	System.out.println("user.getEmail()");	
	String id = null;
		
	try { 
      // Create customer
	  Customer stripeCustomer = Customer.create(customerParams);
	  id = stripeCustomer.getId();
	  System.out.println(stripeCustomer);
	} catch (CardException e) {
	  // Transaction failure
	} catch (RateLimitException e) {
	  // Too many requests made to the API too quickly
	} catch (InvalidRequestException e) {
	  // Invalid parameters were supplied to Stripe's API
	} catch (AuthenticationException e) {
	  // Authentication with Stripe's API failed (wrong API key?)
	} catch (APIConnectionException e) {
	  // Network communication with Stripe failed
	} catch (StripeException e) {
	  // Generic error
	} catch (Exception e) {
	// Something else happened unrelated to Stripe
	}
	
    return id;	
  }

  public void chargeCreditCard(Subscription sub) {
			
    // Stripe requires the charge amount to be in cents
    int chargeAmountCents = Math.toIntExact(sub.getAmount());
    AppUser user=userrep.findByUserId(4);
    System.out.println(user);
    System.out.println(user.getIdStrype());
    System.out.println(chargeAmountCents);
	Map<String, Object> chargeParams = new HashMap<String, Object>();
	chargeParams.put("amount", chargeAmountCents);
	chargeParams.put("currency", "usd");
	chargeParams.put("description", "Monthly Charges");		
	chargeParams.put("customer", user.getIdStrype());
			
	try {
	  // Submit charge to credit card 
		System.out.println("wselna lenna");
		System.out.println(chargeParams);
	  Charge charge = Charge.create(chargeParams);
      System.out.println(charge);
    } catch (CardException e) {
	  // Transaction was declined
	  System.out.println("Status is: " + e.getCode());
	  System.out.println("Message is: " + e.getMessage());
	} catch (RateLimitException e) {
	  // Too many requests made to the API too quickly
	} catch (InvalidRequestException e) {
	  // Invalid parameters were supplied to Stripe's API
    } catch (AuthenticationException e) {
	  // Authentication with Stripe's API failed (wrong API key?)
	} catch (APIConnectionException e) {
	  // Network communication with Stripe failed
	 } catch (StripeException e) {
	  // Generic error
	} catch (Exception e) {
	  // Something else happened unrelated to Stripe
	}	
  }
}