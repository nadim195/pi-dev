package tn.dari.spring.service.interfaces;

import java.util.Set;

import tn.dari.spring.domain.Announcement;

public interface UserHistoryService {
		public void addToHistory(int iduser,int idann);
		public void deleteFromHistory(int iduser,int idann);
		public Set<Announcement> fetchAllhistory(int iduser);
}
