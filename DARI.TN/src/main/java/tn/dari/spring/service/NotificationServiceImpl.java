package tn.dari.spring.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Notification;
import tn.dari.spring.domain.NotificationType;
import tn.dari.spring.domain.Profile;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.NotificationRepository;
import tn.dari.spring.service.interfaces.FavouriteAnnoucementService;
import tn.dari.spring.service.interfaces.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	AppUserRepository userRep;
	@Autowired
	NotificationRepository notifRep;
	@Autowired
	FavouriteAnnoucementService favService;
	
	
	//for testing only
	@Override
	public Notification add(Notification notification, int userId, int announcementId) {
		notification.setUser(userRep.findById(userId).get());
		notification.setAnnouncement(annRep.findById(announcementId).get());
		notification.setIsRead(false);
		notifRep.save(notification);
		return notification;
	}

	@Override
	public int delete(Notification notification) {
		notifRep.delete(notification);
		return 1;
	}

	@Override
	public int deleteById(int notificationId) {
		notifRep.deleteById(notificationId);
		return 1;
	}

	@Override
	public Notification update(Notification notification, int id) {
		notification.setUser(notifRep.findById(id).get().getUser());
		notification.setAnnouncement(notifRep.findById(id).get().getAnnouncement());
		notification.setNotificationId(id);
		notifRep.save(notification);
		return notification;
	}

	@Override
	public List<Notification> fetchAll() {
		return (List<Notification>) notifRep.findAll();
	}
	
	@Override
	public List<Notification> fetchUnread() {
		return notifRep.findByIsReadFalse();
	}
	
	@Override
	public List<Notification> fetchUnreadByUser(int userId) {
		return notifRep.findByUser(userRep.findByUserId(userId)).stream().filter(n -> !n.isRead()).collect(Collectors.toList());
	}
	
	//call when user clicks on notification
	@Override
	public void read(int id){
		Notification notification = notifRep.findById(id).get();
		notification.setIsRead(true);
		notifRep.save(notification);
	}
	//same
	@Override
	public void read(Notification notification){
		notification.setIsRead(true);
		notifRep.save(notification);
	}
	
	//marks notification as unread
	@Override
	public void unRead(int id){
		Notification notification = notifRep.findById(id).get();
		notification.setIsRead(false);
		notifRep.save(notification);
	}
	//same
	@Override
	public void unRead(Notification notification){
		notification.setIsRead(false);
		notifRep.save(notification);
	}
	
	//Marks all users notifications as read
	@Override
	public void readAll(AppUser user){
		notifRep.findByUser(user).stream().forEach(n -> {
			n.setIsRead(true);
			notifRep.save(n);
		});
	}
	
	//call when creating PriceHistory
	@Override
	public int create(double priceChange, int announcementId) {
		if(priceChange==0)
			return 0;
		NotificationType type;
		int count=0;
		if(priceChange<0)
			type = NotificationType.PriceDecreace;
		else
			type = NotificationType.PriceIncrease;
		List<AppUser> users = favService.fetchUsersByFavouriteAnnouncement(announcementId);
		Announcement announcement = annRep.findById(announcementId).get();
		for (AppUser user : users) {
			notifRep.save(new Notification(type,user,announcement));
			count++;
		}
		return count;
	}

	
	//call when creating RentHistory or SaleHistory
	@Override
	public int create(NotificationType type, int announcementId) {
		int count=0;
		List<AppUser> users = favService.fetchUsersByFavouriteAnnouncement(announcementId);
		Announcement announcement = annRep.findById(announcementId).get();
		for (AppUser user : users) {
			notifRep.save(new Notification(type,user,announcement));
			count++;
		}
		return count;
	}

}
