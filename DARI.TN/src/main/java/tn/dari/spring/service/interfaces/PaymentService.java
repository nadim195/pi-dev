package tn.dari.spring.service.interfaces;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Subscription;

public interface PaymentService {
  public String createCustomer(AppUser user);
  public void chargeCreditCard(Subscription sub);
}