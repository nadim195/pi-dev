package tn.dari.spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Profile;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.service.interfaces.AppUserService;
import tn.dari.spring.service.interfaces.FavouriteAnnoucementService;

@Service
public class FavouriteAnnouncementServiceImpl implements FavouriteAnnoucementService {

	@Autowired
	AppUserRepository appRep;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	AppUserService appServ;

	@Override
	public void addToFavourite(int iduser, int idann) {
		AppUser user = appRep.findById(iduser).get();
		Announcement a = annRep.findById(idann).get();
		if (a == null)
			throw new RuntimeException("Announcement not found");
		if (user == null)
			throw new RuntimeException("user not found");
		
		Set<Announcement> favs = user.getFavouritesAnnouncements();
		if(Objects.isNull(favs))
			favs = new HashSet<Announcement>();
		favs.add(a);
		user.setFavouritesAnnouncements(favs);
		appRep.save(user);
	}

	@Override
	public void deleteFromFavourite(int iduser, int idann) {
		AppUser user = appRep.findById(iduser).get();
		Announcement a = annRep.findById(idann).get();
		if (a == null)
			throw new RuntimeException("Announcement not found");
		if (user == null)
			throw new RuntimeException("user not found");
		Set<Announcement> favs = new HashSet<Announcement>();
		favs = user.getFavouritesAnnouncements();
		favs.remove(a);
		user.setFavouritesAnnouncements(favs);
		appRep.save(user);
	}

	@Override
	public java.util.Set<Announcement> fetchFavouriteAnnouncements(int iduser) {
		AppUser user = appRep.findById(iduser).get();
		if (user == null)
			throw new RuntimeException("user not found");
		return user.getFavouritesAnnouncements();
	}

	@Override
	public Set<Profile> fetchProfilesByFavouriteAnnouncement(int annId) {
		Announcement a = annRep.findById(annId).get();
		if (a == null)
			throw new RuntimeException("Announcement not found");
		Set<Profile> profiles = new HashSet<Profile>();
		ArrayList<AppUser> users = new ArrayList<AppUser>();
		users = appServ.getUsers();
		for (AppUser user : users) {
			if (user.getFavouritesAnnouncements().contains(a))
				profiles.add(user.getProfile());
		}
		return profiles;
	}
	
	@Override
	public List<AppUser> fetchUsersByFavouriteAnnouncement(int annId) {
		Announcement a = annRep.findById(annId).get();
		if (a == null)
			throw new RuntimeException("Announcement not found");
		//List<AppUser> users = new HashSet<AppUser>();
		ArrayList<AppUser> users2 = new ArrayList<AppUser>();
		List<AppUser> users = appServ.getUsers();
		for (AppUser user : users) {
			if (user.getFavouritesAnnouncements().contains(a))
				users2.add(user);
		}
		return users2;
	}

}
