package tn.dari.spring.service;

import tn.dari.spring.dto.CreditDTO;
import tn.dari.spring.dto.SimulatePayload;

import java.util.List;

public interface SimulationService {

    List<CreditDTO> simulate(SimulatePayload payload);

}
