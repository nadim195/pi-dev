package tn.dari.spring.service;

import tn.dari.spring.domain.AppUser;
import java.time.Instant;
import java.time.Period;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Subscription;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.SubscriptionRepository;
import tn.dari.spring.service.interfaces.SubscriptionService;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
	
	@Autowired
	private SubscriptionRepository subrepo;
	
	@Autowired
	private AppUserRepository userrep;

	
	
	@Override
	public void ActivateSubscription(Subscription sub, int userid){
		
		AppUser user=userrep.findByUserId(userid);
		user.setSubscribed(true);
		sub.setStartDate(Date.from(Instant.now()));
		sub.setIsactive(false);
		Date endDate = Date.from(Instant.now());
		if(sub.getSubType().equals("normal")) 
		{
			endDate = Date.from(Instant.now().plus(Period.ofDays(30)));
			user.setRole(new Role("NORMAL_SUBSCRIBER"));
		}
		if(sub.getSubType().equals("gold")) 
		{
			endDate = Date.from(Instant.now().plus(Period.ofDays(90)));
			user.setRole(new Role("GOLDEN_SUBSCRIBER"));
		}
		if(sub.getSubType().equals("platinium")) 
		{
			endDate = Date.from(Instant.now().plus(Period.ofDays(180)));
		}
		
			user.setRole(new Role("PLATINIUM_SUBSCRIBER"));
		}
		userrep.save(user);
		sub.setEndDate(endDate);
		sub.setUser(user);
		subrepo.save(sub);
	}
	
	
	@Override
	public Subscription updateSubscription(Subscription s, int id) {
		
		Subscription sub = subrepo.findById(id).get();
		
		s.setEndDate(sub.getEndDate());
		return subrepo.save(s);
	}
	
	
	@Override
	public Subscription getSubscriptionById(int id) {
		
		return null;
	}
	
	
	@Override
	public java.util.List<Subscription> getAllSubscriptions() {
		
		return null;
	}
	
	
	@Override
	public java.util.List<Subscription> filterSub(String type) {
		return null;
	}


	@Override
	public Subscription LoadByUserId(int userid) {
		
		//return subrepo.findByUserId(userid);
		return null;
	}
	
}