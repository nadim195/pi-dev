package tn.dari.spring.service.interfaces;

import java.util.List;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Criteria;

public interface CriteriaService {

	public Criteria add(Criteria criteria, int profileId);
	public int delete(Criteria criteria);
	public int deleteById(int criteriaId);
	public Criteria update(Criteria criteria, int criteriaId);
	public List<Criteria> fetchAll();
	public List<Criteria> fetchAllByUser(AppUser user);
	public Criteria fetchByName(String name, int userId);
	public boolean testAnnouncementByOneCriteria(Criteria criteria, Announcement announcement);
	public List<Announcement> filterAnnouncementsByOneCriteria(Criteria criteria, List<Announcement> announcements);
	public List<Announcement> filterAnnouncementsByMutipleCriteria(List<Criteria> criteria, List<Announcement> announcements);
	public List<Announcement> filterAnnouncementsByUserCriteria(List<Announcement> announcements, AppUser user);
	public List<Announcement> filterAllAnnouncementsByUserCriteria(AppUser user);
	public List<Announcement> search(Criteria criteria);
	public List<Announcement> search();
	public List<Announcement> search(String name, int userId);
	public List<Announcement> search(int userId);
	

	
}
