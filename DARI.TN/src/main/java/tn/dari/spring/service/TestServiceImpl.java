package tn.dari.spring.service;

import tn.dari.spring.domain.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.repository.RoleRepository;
import tn.dari.spring.service.interfaces.TestService;

@Service
public class TestServiceImpl implements TestService {

	@Autowired
	RoleRepository roleRep;

	@Override
	public Role addRole(Role role) {
		roleRep.save(role);
		return role;
	}

}
