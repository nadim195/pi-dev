package tn.dari.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.ZonePrice;
import tn.dari.spring.repository.ZonePriceRepository;
import tn.dari.spring.service.interfaces.ZonePriceService;

@Service
public class ZonePriceServiceImpl implements ZonePriceService {
	
	@Autowired
	ZonePriceRepository zoneRep;

	@Override
	public ZonePrice add(ZonePrice zonePrice) {
		zoneRep.save(zonePrice);
		return zonePrice;
	}

	@Override
	public int delete(ZonePrice zonePrice) {
		zoneRep.delete(zonePrice);
		return 1;
	}

	@Override
	public int deleteById(int id) {
		zoneRep.deleteById(id);
		return 1;
	}

	@Override
	public ZonePrice update(ZonePrice zonePrice, int id) {
		zonePrice.setZoneId(id);
		zoneRep.save(zonePrice);
		return zonePrice;
	}

	@Override
	public List<ZonePrice> fetchAll() {
		return (List<ZonePrice>) zoneRep.findAll();
	}

}
