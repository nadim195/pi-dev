package tn.dari.spring.service;


import java.util.List;

import tn.dari.spring.domain.Furniture;



public interface FurnitureService {

	int AddFurniture(Furniture furniture, int userId);
	void UpdateF(Furniture furn, int id);
	void DeleteFurniture(int id);

	int countFurnitures();
	List<Furniture> loadAllActive();
	List<Furniture> loadFurnitureByCategory(String category);
	List<Furniture> loadFurnitureByFurnitureName(String FurnitureName);
	//List<Furniture> loadFurnitureByFurniturePrice(double FurniturePrice);
	//List<Furniture> loadFurnitureByid(int id);
	List<Furniture> loadAllFurnitures();
	List<Furniture> loadVerifiedFurnitures();
	List<Furniture> loadNotVerifiedFurnitures();
	List<Furniture> loadFurnituresByNumberOfPieces(int NumberOfPieces);
	//void UnActiveFurniture(int id);
	void UnActiveFurniture(Furniture furn, int id);
	void UnActive(int id);

	


	
}
