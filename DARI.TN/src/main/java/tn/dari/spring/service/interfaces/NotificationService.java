package tn.dari.spring.service.interfaces;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Notification;
import tn.dari.spring.domain.NotificationType;
import java.util.List;

public interface NotificationService {

	public Notification add(Notification notification, int userId, int announcementId);
	public Notification update(Notification notification, int id);
	public int delete(Notification notification);
	public int deleteById(int id);
	public int create(double PriceChange, int announcementId);
	public int create(NotificationType type, int announcementId);
	public void read(int id);
	public void read(Notification notification);
	public void unRead(int id);
	public void unRead(Notification notification);
	public void readAll(AppUser user);
	public List<Notification> fetchAll();
	public List<Notification> fetchUnreadByUser(int userId);
	public List<Notification> fetchUnread();
}
