package tn.dari.spring.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Availability;
import tn.dari.spring.domain.ConfirmationToken;
import tn.dari.spring.domain.Meeting;
import tn.dari.spring.domain.Profile;
import tn.dari.spring.domain.Report;
import tn.dari.spring.domain.Role;
import tn.dari.spring.domain.util.AppUserForm;
import tn.dari.spring.domain.util.MeetingForm;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.AvailabilityRepository;
import tn.dari.spring.repository.ConfirmationTokenRepository;
import tn.dari.spring.repository.MeetingRepository;
import tn.dari.spring.repository.ProfileRepository;
import tn.dari.spring.repository.ReportRepository;
import tn.dari.spring.repository.RoleRepository;
import tn.dari.spring.service.interfaces.AppUserService;

@Service
public class AppUserServiceImpl implements AppUserService {

	@Autowired
	AppUserRepository userRep;
	@Autowired
	RoleRepository roleRep;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	ProfileRepository profileRep;
	@Autowired
	ConfirmationTokenRepository confRep;
	@Autowired
	AvailabilityRepository availabilityRep;
	@Autowired
	MeetingRepository meetingRep;
	@Autowired
	ReportRepository reportRep;
	@Autowired
	AnnouncementRepository annRep;

	@Override
	public AppUser loadUserByEmail(String email) {
		return userRep.findByEmail(email);
	}

	@Override
	public AppUser addSimpleUser(AppUserForm a) {
		if (userRep.findByEmail(a.getEmail()) != null)
			throw new RuntimeException("User already exists");
		Role r = roleRep.findByRoleName("SIMPLE_USER");
		AppUser user = new AppUser();
		Profile profile = new Profile();
		
		
		if (a.getAdress() != null) {
			user.setAdress(a.getAdress());
		}
		if (a.getEmail() != null) {
			user.setEmail(a.getEmail());
		}
		if (a.getFirstName() != null) {
			user.setFirstName(a.getFirstName());
		}
		if (a.getLastName() != null) {
			user.setLastName(a.getLastName());
		}
		if (a.getPassword() != null) {
			user.setPassword(bCryptPasswordEncoder.encode(a.getPassword()));
		}
		if (a.getPhoneNumber() != 0) {
			user.setPhoneNumber(a.getPhoneNumber());
		}
		user.setVerified(false);
		user.setCreatedAt(Date.from(Instant.now()));
		user.setUpdatedAt(Date.from(Instant.now()));
		ArrayList<Role> roles = new ArrayList<Role>();
		roles.add(r);
		user.setRole(r);
		profileRep.save(profile);
		user.setProfile(profile);
		userRep.save(user);
		return user;
	}

	@Override
	public void deleteUser(int id) {
		AppUser appUser = userRep.findByUserId(id);
		if (appUser == null)
			throw new RuntimeException("User doesn't exists");
		appUser.setRole(null);
		userRep.save(appUser);
		ConfirmationToken conf = confRep.findByUser(appUser);
		confRep.delete(conf);
		userRep.delete(appUser);

	}

	@Override
	public ArrayList<AppUser> getUsers() {
		return (ArrayList<AppUser>) userRep.findAll();
	}

	@Override
	public ArrayList<AppUser> getSimpleUsers() {
		ArrayList<AppUser> users = this.getUsers();
		ArrayList<AppUser> simpleUsers = new ArrayList<AppUser>();

		for (AppUser au : users) {
			if (au.getRole().getRoleName().equals("SIMPLE_USER"))
				simpleUsers.add(au);
		}
		return simpleUsers;

	}

	@Override
	public ArrayList<AppUser> getSubscribedUsers() {
		ArrayList<AppUser> users = this.getUsers();
		ArrayList<AppUser> subscriberUsers = new ArrayList<AppUser>();

		for (AppUser au : users) {
			if (au.getRole().getRoleName().equals("SUBSCRIBER_USER"))
				subscriberUsers.add(au);
		}
		return subscriberUsers;
	}

	@Override
	public AppUser updateUser(AppUserForm a) {
		AppUser user = userRep.findByEmail(a.getEmail());
		if (user == null)
			throw new RuntimeException("User doesn't exists");
		if (a.getAdress() != null) {
			user.setAdress(a.getAdress());
		}
		if (a.getEmail() != null) {
			user.setEmail(a.getEmail());
		}
		if (a.getFirstName() != null) {
			user.setFirstName(a.getFirstName());
		}
		if (a.getLastName() != null) {
			user.setLastName(a.getLastName());
		}
		if (a.getPassword() != null) {
			user.setPassword(a.getPassword());
		}
		if (a.getPhoneNumber() != 0) {
			user.setPhoneNumber(a.getPhoneNumber());
		}
		if (a.isVerified()) {
			user.setVerified(true);
		}
		user.setUpdatedAt(Date.from(Instant.now()));
		userRep.save(user);
		return user;
	}

	@Override
	public AppUser addAdmin(AppUser a) {
		if (userRep.findByEmail(a.getEmail()) != null)
			throw new RuntimeException("User already exists");
		Role r = roleRep.findByRoleName("ADMIN");
		a.setRole(r);
		String pwd = bCryptPasswordEncoder.encode(a.getPassword());
		a.setPassword(pwd);
		a.setCreatedAt(Date.from(Instant.now()));
		a.setUpdatedAt(Date.from(Instant.now()));
		userRep.save(a);
		return a;
	}

	@Override
	public Role addRole(Role role) {
		roleRep.save(role);
		return role;
	}

	@Override
	public AppUser addSubscribedUser(AppUser a) {
		if (userRep.findByEmail(a.getEmail()) != null)
			throw new RuntimeException("User already exists");
		Role r1 = roleRep.findByRoleName("SUBSCRIBED_USER");
		a.setRole(r1);
		a.setCreatedAt(Date.from(Instant.now()));
		a.setUpdatedAt(Date.from(Instant.now()));
		String pwd = bCryptPasswordEncoder.encode(a.getPassword());
		a.setPassword(pwd);
		a.setSubscribed(true);
		userRep.save(a);
		return a;
	}

	@Override
	public int usersCount() {
		return this.getUsers().size();
	}

	@Override
	public int simpleUsersCount() {
		return this.getSimpleUsers().size();
	}

	@Override
	public int subscriberUsersCount() {
		return this.getSubscribedUsers().size();
	}

	@Override
	public int userCountPerDate(Date d) {
		int count = 0;
		for (AppUser u : this.getUsers()) {
			if (u.getCreatedAt().getDate() == d.getDate())
				count++;
		}
		return count;
	}

	@SuppressWarnings("deprecation")
	@Override
	public int newSimpleUsersToday() {
		int count = 0;
		for (AppUser u : this.getSimpleUsers()) {
			if (u.getCreatedAt() != null) {
				if (u.getCreatedAt().getDay() == Date.from(Instant.now()).getDay())
					count++;
			}
		}
		return count;
	}

	@Override
	public int newSubscribersToday() {
		int count = 0;
		for (AppUser u : this.getSubscribedUsers()) {
			if (u.getCreatedAt() != null) {
				if (u.getCreatedAt().getDay() == Date.from(Instant.now()).getDay())
					count++;
			}
		}
		return count;
	}

	@Override
	public int newSimpleUsersThisMonth() {
		int count = 0;
		for (AppUser u : this.getSimpleUsers()) {
			if (u.getCreatedAt() != null) {
				if (u.getCreatedAt().getMonth() == Date.from(Instant.now()).getMonth())
					count++;
			}
		}
		return count;
	}

	@Override
	public int newSubscribersThisMonth() {
		int count = 0;
		for (AppUser u : this.getSubscribedUsers()) {
			if (u.getCreatedAt() != null) {
				if (u.getCreatedAt().getMonth() == Date.from(Instant.now()).getMonth())
					count++;
			}
		}
		return count;
	}

 	@Override
	public void addAvailability(Availability a, int userId) {
		AppUser user = userRep.findById(userId).get();
		if (user == null)
			throw new RuntimeException("User doesnt exists");
		Set<Availability> av = new HashSet<Availability>();
		av = user.getAvailibility();
		av.add(a);
		user.setAvailibility(av);
		userRep.save(user);
		a.setUser(user);
		availabilityRep.save(a);
	}

	@Override
	public void requestMeeting(int currentUserId,int userId,MeetingForm meet) {
		AppUser currentUser = userRep.findById(currentUserId).get();
		AppUser user = userRep.findById(userId).get();
	
	 
	 /*   Date date1=null;
		try {
			date1 = new SimpleDateFormat("yyyy-mm-dd").parse(meet.getDate());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.print("gggggggggggg"+date1);
		if (user == null || currentUser==null )
			throw new RuntimeException("User doesnt exists");
		if(availabilityRep.findByDateAndUser(date1,user)==null)
		{	System.out.println(availabilityRep.findByDateAndUser(date1,user));
				// meet.getFromHour()<availabilityRep.findByDateAndUser(meet.getDate(),user).getFromHour()||
				// meet.getToHour()>availabilityRep.findByDateAndUser(meet.getDate(),user).getToHour()) 
		throw new RuntimeException("user not available");
		} */
		Meeting meeting = new Meeting();
		meeting.setClient(currentUser);
		meeting.setUser(user);
		meeting.setMeetingDate(meet.getDate());
		meeting.setFromHour(meet.getFromHour());
		meeting.setToHour(meet.getToHour());
		meetingRep.save(meeting);
	}

	@Override
	public Meeting confirmMeeting(int userId, int requestUserId) {
		AppUser requestUser = userRep.findById(requestUserId).get();
		AppUser user = userRep.findById(userId).get();
		if (user == null || requestUser==null )
			throw new RuntimeException("User doesnt exists");
		Meeting meet = meetingRep.findByClientAndUser(requestUser, user);
		if(meet!=null) {
			meet.setConfirmed(true);
			meetingRep.save(meet);
		}
		return meet;
		}

	@Override
	public void reportUser(int userId, int reportedUserId) {
		AppUser reportedUser = userRep.findById(reportedUserId).get();
		AppUser user = userRep.findById(userId).get();
		if (user == null || reportedUser==null )
			throw new RuntimeException("User doesnt exists");
		Report report=new Report();
		report.setReportedUser(reportedUser);
		report.setReportingUser(user);
		int signal = reportedUser.getReportsNumber()+1;
		reportedUser.setReportsNumber(signal);
		userRep.save(reportedUser);
		reportRep.save(report);
	}

	@Override
	public List<Meeting> requestedMeetings(int userId) {
		List<Meeting> meets=new ArrayList<Meeting>();
		for(Meeting m:meetingRep.findAll())
		{
			if(m.getUser().getUserId()==userId&&m.isConfirmed()==false)
			{
				meets.add(m);
			}
		}
		return meets;
	}

	@Override
	public void validateAnnoucement(int annId) {
		Announcement a=annRep.findById(annId).get();
		a.setVerified(true);
		annRep.save(a);
		
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		AppUser appUser = loadUserByEmail(username);
		if (appUser == null)
			throw new UsernameNotFoundException("invalid user");
		return appUser;
	}
}

