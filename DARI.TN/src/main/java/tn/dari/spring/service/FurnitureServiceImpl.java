package tn.dari.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Furniture;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.FurnitureRepository;
import tn.dari.spring.service.FurnitureService;
@Service
public class FurnitureServiceImpl implements FurnitureService{
	@Autowired
	private FurnitureRepository furnitureRepository;
	@Autowired
	AppUserRepository userRep;
	@Autowired
	FurnitureService furnitureService;
	
	@Override
	public List<Furniture> loadAllActive(){
		List<Furniture> furniture = furnitureRepository.findByActiveTrue();
		furniture.removeIf(a -> a.getVerified()==false);
		return furniture;
	}
	
	@Override
	public List<Furniture> loadFurnitureByCategory(String category) {
		return furnitureRepository.findByCategory(category);
	}

	@Override
	public List<Furniture> loadFurnitureByFurnitureName(String FurnitureName) {
		return furnitureRepository.findByFurnitureName(FurnitureName);
	}

//	@Override
//	public List<Furniture> loadFurnitureByFurniturePrice(double FurniturePrice) {
//		return furnitureRepository.findByFurniturePrice(FurniturePrice);
//	}

//	@Override
//	public List<Furniture> loadFurnitureByid(int id) {
//		return furnitureRepository.findByid(id);
//	}

	@Override
	public int AddFurniture(Furniture furniture, int userId) {
		furniture.setUser(userRep.findByUserId(userId));
		furnitureRepository.save(furniture);
//	furnitureService.furniturePriceUpdate(furniture);
		return furniture.getId();
	}
	
	
	@Override
	public List<Furniture> loadFurnituresByNumberOfPieces(int NumberOfPieces) {
		return (List<Furniture>) furnitureRepository.findByNumberOfPieces(NumberOfPieces);
	}
	
	
	
	
	public void UpdateF(Furniture furniture,int Id)   
	{  
		Furniture furn = furnitureRepository.findByid(Id);
		furniture.getFurnitureName();
		if (furniture.getFurnitureName() != null) {
			furn.setFurnitureName(furniture.getFurnitureName());	
		};
		
	//	furniture.setFurnitureName(furn.getFurnitureName());
		furniture.setVerified(furn.getVerified());
		furniture.setCategory(furn.getCategory());
		furniture.setDescription(furn.getDescription());
		furniture.setFabricator(furn.getFabricator());
		furniture.setUser(furn.getUser());
		furniture.setFurniturePrice(furn.getFurniturePrice());
		furniture.setInStockNumber(furn.getInStockNumber());
		furniture.setNumberOfPieces(furn.getNumberOfPieces());
		
		furnitureRepository.save(furn);
	}  

	@Override
	public void DeleteFurniture(int id) {
		furnitureRepository.deleteById(id);
	}

	@Override
	public int countFurnitures() {
		List<Furniture> announcements = (List<Furniture>) furnitureRepository.findAll();
		return announcements.size();
	}

	
	@Override
	public List<Furniture> loadVerifiedFurnitures() {
		return (List<Furniture>) furnitureRepository.findByVerifiedTrue();
	}

	@Override
	public List<Furniture> loadNotVerifiedFurnitures() {
		return (List<Furniture>) furnitureRepository.findByVerifiedFalse();
	}
	
	@Override
	public List<Furniture> loadAllFurnitures() {
		return (List<Furniture>) furnitureRepository.findAll();
	}

	@Override
	public void UnActiveFurniture(Furniture furniture,int Id)   
	{  
		Furniture furn = furnitureRepository.findByid(Id);
		furniture.getFurnitureName();
		if (furniture.getFurnitureName() != null) {
			furn.setActive(false);
			
		};
	
		
		furnitureRepository.save(furn);
	} 


	
	public void UnActive(int Id)   
	{  
		Furniture furn = furnitureRepository.findByid(Id);
		
			furn.setActive(false);
		
		furnitureRepository.save(furn);
	} 
	
	
}
