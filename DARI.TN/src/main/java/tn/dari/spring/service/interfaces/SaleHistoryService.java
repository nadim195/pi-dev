package tn.dari.spring.service.interfaces;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.SaleHistory;
import java.util.List;

public interface SaleHistoryService {

	public List<SaleHistory> fetchAll();
	public SaleHistory update(SaleHistory saleHistory, int id);
	public int deleteById(int id);
	public int delete(SaleHistory saleHistory);
	public SaleHistory add(SaleHistory saleHistory, int announcementId);
	public SaleHistory fetchOne(int id);
	public void markSold(int announcementId);
	public void markSold(Announcement announcement);

}
