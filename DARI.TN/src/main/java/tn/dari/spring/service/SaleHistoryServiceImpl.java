package tn.dari.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.NotificationType;
import tn.dari.spring.domain.RentHistory;
import tn.dari.spring.domain.SaleHistory;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.SaleHistoryRepository;
import tn.dari.spring.service.interfaces.NotificationService;
import tn.dari.spring.service.interfaces.SaleHistoryService;

@Service
public class SaleHistoryServiceImpl implements SaleHistoryService {
	
	@Autowired
	SaleHistoryRepository saleRep;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	NotificationService notifService;

	@Override
	public void markSold(int announcementId) {
		Announcement announcement = annRep.findById(announcementId).get();
		announcement.setArchived(true);
		SaleHistory saleHistory = new SaleHistory(announcement);
		saleRep.save(saleHistory);
		notifService.create(NotificationType.FavoriteSold, announcementId);
	}
	
	@Override
	public void markSold(Announcement announcement) {
		announcement.setArchived(true);
		SaleHistory saleHistory = new SaleHistory(announcement);
		saleRep.save(saleHistory);
		notifService.create(NotificationType.FavoriteSold, announcement.getAnnouncementId());
	}
	
	@Override
	public SaleHistory add(SaleHistory saleHistory, int announcementId) {
		saleHistory.setAnnouncement(annRep.findById(announcementId).get());
		saleRep.save(saleHistory);
		notifService.create(NotificationType.FavoriteSold, announcementId);
		return saleHistory;
	}

	@Override
	public int delete(SaleHistory saleHistory) {
		saleRep.delete(saleHistory);
		return 1;
	}

	@Override
	public int deleteById(int id) {
		saleRep.deleteById(id);
		return 1;
	}

	@Override
	public SaleHistory update(SaleHistory saleHistory, int id) {
		saleHistory.setAnnouncement(saleRep.findById(id).get().getAnnouncement());
		saleHistory.setId(id);
		saleRep.save(saleHistory);
		return saleHistory;
	}

	@Override
	public List<SaleHistory> fetchAll() {
		return (List<SaleHistory>) saleRep.findAll();
	}
	
	@Override
	public SaleHistory fetchOne(int id) {
		return saleRep.findById(id).get();
	}
	

}
