package tn.dari.spring.service;

import tn.dari.spring.domain.Credit;
import tn.dari.spring.dto.CreditDTO;

public interface CreditService extends GenericService<Credit, CreditDTO, Long> {
}
