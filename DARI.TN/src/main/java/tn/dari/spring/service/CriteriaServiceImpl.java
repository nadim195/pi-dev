package tn.dari.spring.service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Criteria;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.CriteriaRepository;
import tn.dari.spring.repository.ProfileRepository;
import tn.dari.spring.service.interfaces.AnnouncementService;
import tn.dari.spring.service.interfaces.CriteriaService;

@Service
public class CriteriaServiceImpl implements CriteriaService {
	
	@Autowired
	AnnouncementService annService;
	@Autowired
	CriteriaRepository criteriaRep;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	AppUserRepository userRep;

	@Override
	public Criteria add(Criteria criteria, int userId) {
		if(nameExists(criteria.getCriteriaName()))
			return null;
		criteria.setUser(userRep.findById(userId).get());
		criteriaRep.save(criteria);
		return criteria;
	}

	@Override
	public int delete(Criteria criteria) {
		criteriaRep.delete(criteria);
		return 1;
	}

	@Override
	public int deleteById(int criteriaId) {
		criteriaRep.deleteById(criteriaId);
		return 1;
	}

	@Override
	public Criteria update(Criteria criteria, int id) {
		Criteria oldCriterria = criteriaRep.findById(id).get();
		if(nameExists(criteria.getCriteriaName()) && !oldCriterria.getCriteriaName().equals(criteria.getCriteriaName()))
			return null;
		criteria.setUser(oldCriterria.getUser());
		criteria.setCriteriaId(id);
		criteriaRep.save(criteria);
		return criteria;
	}

	//For Testing only
	@Override
	public List<Criteria> fetchAll() {
		return (List<Criteria>) criteriaRep.findAll();
	}

	//Get Criteria By Profile
	@Override
	public List<Criteria> fetchAllByUser(AppUser user) {
		return criteriaRep.findByUser(user);
	}

	//Test one announcements by one criteria
	@Override
	public boolean testAnnouncementByOneCriteria(Criteria criteria, Announcement announcement) {
		if(criteria.getMaxNumberOfFloors()!=0 && announcement.getNumberOfFloors()>criteria.getMaxNumberOfFloors())
			return false;
		if(criteria.getMinNumberOfFloors()!=0 && announcement.getNumberOfFloors()<criteria.getMinNumberOfFloors())
			return false;
		if(criteria.getMaxNumberOfRooms()!=0 && announcement.getNumberOfRooms()>criteria.getMaxNumberOfRooms())
			return false;
		if(criteria.getMinNumberOfRooms()!=0 && announcement.getNumberOfRooms()<criteria.getMinNumberOfRooms())
			return false;
		
		if(criteria.getAnnouncementType()!=null)
			if(!criteria.getAnnouncementType().equals(announcement.getType()))
				return false;
		if(criteria.getEstateType()!=null)
			if(!criteria.getEstateType().equals(announcement.getEstateType()))
				return false;
		if(criteria.getLocation()!=null)
			if(!criteria.getLocation().equals(announcement.getLocation()))
				return false;
		
		if(criteria.getMaxPrice()!=0 && announcement.getPrice()>criteria.getMaxPrice())
			return false;
		if(criteria.getMinPrice()!=0 && announcement.getPrice()<criteria.getMinPrice())
			return false;
		if(criteria.getMaxSurface()!=0 && announcement.getSurface()>criteria.getMaxSurface())
			return false;
		if(criteria.getMinSurface()!=0 && announcement.getSurface()<criteria.getMinSurface())
			return false;
		return true;
	}
	
	//Filter multiple announcements by one criteria
	@Override
	public List<Announcement> filterAnnouncementsByOneCriteria(Criteria criteria, List<Announcement> announcements) {
		return announcements.stream().filter(a -> testAnnouncementByOneCriteria(criteria,a)).collect(Collectors.toList());
	}
	
	//Test one announcement by multiple criteria
	private boolean testAnnouncementByMutipleCriteria(Announcement announcement, List<Criteria> criteria){
		for (Criteria crt : criteria) {
			if (testAnnouncementByOneCriteria(crt,announcement))
				return true;
		}
		return false;
	}
	
	//ensures unique criteria name per profile
	//call when adding new criteria or updating criteria name
	private Boolean nameExists(String name){
		return criteriaRep.findByCriteriaName(name)!=null;
	}

	//Filter multiple announcements by multiple criteria
	@Override
	public List<Announcement> filterAnnouncementsByMutipleCriteria(List<Criteria> criteria, List<Announcement> announcements) {
		return announcements.stream().filter(a -> testAnnouncementByMutipleCriteria(a,criteria)).collect(Collectors.toList());
	}
	
	//Filter multiple announcements by all user Criteria
	@Override
	public List<Announcement> filterAnnouncementsByUserCriteria(List<Announcement> announcements, AppUser user) {
		return filterAnnouncementsByMutipleCriteria(fetchAllByUser(user),announcements);
	}
	
	//Filter All announcements by all user Criteria
	@Override
	public List<Announcement> filterAllAnnouncementsByUserCriteria(AppUser user) {
		return filterAnnouncementsByMutipleCriteria(fetchAllByUser(user),(List<Announcement>) annRep.findAll());
	}
	
	//find criteria by name and profile
	@Override
	public Criteria fetchByName(String name, int userId) {
		List<Criteria> criterias = criteriaRep.findByUser(userRep.findByUserId(userId));
		if(Objects.isNull(criterias))
			return null;
		Criteria criteria = criterias.stream().filter(c -> c.getCriteriaName()!=null).filter(c -> c.getCriteriaName().equals(name)).findFirst().get();
		if(Objects.isNull(criteria))
			return null;
		return criteria;
	}
	
	//Search by criteria name
	@Override
	public List<Announcement> search(String name, int userId) {
		Criteria criteria= fetchByName(name,userId);
		if(Objects.isNull(criteria))
			return null;
		return search(criteria);
	}
		
	//Search by criteria
	@Override
	public List<Announcement> search(Criteria criteria){
		return filterAnnouncementsByOneCriteria(criteria,annService.loadAllActive());
	}
	
	//Search all
	@Override
	public List<Announcement> search(){
		return annService.loadAllActive();
	}

	//Search by all user Criteria
	@Override
	
	public List<Announcement> search(int userId) {
		return filterAnnouncementsByUserCriteria(annService.loadAllActive(),userRep.findById(userId).get());
	}

}
