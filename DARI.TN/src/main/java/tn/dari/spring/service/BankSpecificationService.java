package tn.dari.spring.service;

import tn.dari.spring.domain.BankSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import tn.dari.spring.dto.LoanPreferences;

import java.util.List;
import java.util.Optional;

public interface BankSpecificationService {

    BankSpecification save(BankSpecification bankspecification);

    Optional<BankSpecification> find(Long id);

    List<BankSpecification> findAll();

    List<BankSpecification> findAll(Sort sort);

    Page<BankSpecification> findAll(Pageable pageable);

    void delete(Long id);

    void delete(BankSpecification bankspecification);

    void deleteAll();

    long count();

    List<BankSpecification> findByValues(LoanPreferences preferences, Long value1, Long value2);

}