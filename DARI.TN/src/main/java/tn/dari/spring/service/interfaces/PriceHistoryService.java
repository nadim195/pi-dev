package tn.dari.spring.service.interfaces;

import java.time.Period;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Set;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.PriceHistory;
import tn.dari.spring.domain.util.AnnouncementInfo;

public interface PriceHistoryService {
	
	public double add (PriceHistory priceHistory, int announcementId);
	public double announcementPriceUpdate(Announcement announcement);
	public PriceHistory update(PriceHistory priceHistory, int id);
	public int delete(PriceHistory priceHistory);
	public int deleteById(int id);
	public List<PriceHistory> fetchAll();
	public List<PriceHistory> fetchAllByAnnouncement(Announcement announcement);
	public Set<PriceHistory> fetchByPeriod(Announcement announcement, Period period);
	public DoubleSummaryStatistics getSummaryStatisticsOfPeriod(Announcement announcement, Period period);
	public AnnouncementInfo getAnnouncementInfo(Announcement announcement, Period period);
	
}
