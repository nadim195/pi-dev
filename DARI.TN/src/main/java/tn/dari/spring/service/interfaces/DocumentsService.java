package tn.dari.spring.service.interfaces;

import java.util.List;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Documents;

public interface DocumentsService {
	public Documents add(Documents documents, int userId);
	public Documents update(Documents documents, int id);
	public Documents fetchByUser(AppUser appUser);
	public int delete(Documents documents);
	public int deleteById(int id);
	public List<Documents> fetchAll();
	

}
