package tn.dari.spring.service.interfaces;

import java.util.ArrayList;
import java.util.Date;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Availability;
import tn.dari.spring.domain.Meeting;
import tn.dari.spring.domain.Role;
import tn.dari.spring.domain.util.AppUserForm;
import tn.dari.spring.domain.util.MeetingForm;

public interface AppUserService {
	public AppUser loadUserByEmail(String email);

	public AppUser addSimpleUser(AppUserForm a);

	public AppUser addSubscribedUser(AppUser a);

	public AppUser addAdmin(AppUser a);

	public void deleteUser(int id);

	public ArrayList<AppUser> getUsers();

	public ArrayList<AppUser> getSimpleUsers();

	public ArrayList<AppUser> getSubscribedUsers();

	public AppUser updateUser(AppUserForm a);

	public Role addRole(Role role);

	public int usersCount();

	public int simpleUsersCount();

	public int subscriberUsersCount();

	public int newSimpleUsersToday();

	public int newSubscribersToday();

	public int newSimpleUsersThisMonth();

	public int newSubscribersThisMonth();

	public int userCountPerDate(Date d);

	public void addAvailability(Availability a, int userId);

	public void requestMeeting(int currentUserId, int userId, MeetingForm meet);

	public Meeting confirmMeeting(int userId, int requestUserId);

	public void reportUser(int userId, int reportedUserId);

}
