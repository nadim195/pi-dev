package tn.dari.spring.service.impl;

import tn.dari.spring.domain.BankSpecification;
import tn.dari.spring.dto.LoanPreferences;
import tn.dari.spring.repository.BankSpecificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import tn.dari.spring.service.BankSpecificationService;

import java.util.List;
import java.util.Optional;

@Service
public class BankSpecificationServiceImpl implements BankSpecificationService {

    @Autowired
    private BankSpecificationRepository bankspecificationRepository;

    @Override
    public BankSpecification save(BankSpecification bankspecification) {
        return bankspecificationRepository.save(bankspecification);
    }

    @Override
    public Optional<BankSpecification> find(Long id) {
        return bankspecificationRepository.findById(id);
    }

    @Override
    public List<BankSpecification> findAll() {
        return bankspecificationRepository.findAll();
    }

    @Override
    public List<BankSpecification> findAll(Sort sort){
        return bankspecificationRepository.findAll(sort);
    }

    @Override
    public Page<BankSpecification> findAll(Pageable pageable){
        return bankspecificationRepository.findAll(pageable);
    }

    @Override
    public void delete(Long id) {
    bankspecificationRepository.deleteById(id);
    }

    @Override
    public void delete(BankSpecification bankspecification) {
        bankspecificationRepository.delete(bankspecification);
    }

    @Override
    public void deleteAll() {
        bankspecificationRepository.deleteAll();
    }

    @Override
    public long count() {
        return bankspecificationRepository.count();
    }

    @Override
    public List<BankSpecification> findByValues(LoanPreferences preferences, Long value1, Long value2) {
        return bankspecificationRepository.findAllByPreferencesAndValueBetweenOrderByValue(preferences, value1, value2);
    }
}
