package tn.dari.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.AppUser;
import tn.dari.spring.domain.Documents;
import tn.dari.spring.domain.Profile;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.repository.DocumentsRepository;
import tn.dari.spring.repository.ProfileRepository;
import tn.dari.spring.service.interfaces.DocumentsService;

@Service
public class DocumentsServiceImpl implements DocumentsService {
	
	@Autowired
	DocumentsRepository docRep;
	@Autowired
	AppUserRepository userRep;

	@Override
	public Documents add(Documents documents, int userId) {
		documents.setUser(userRep.findById(userId).get());
		docRep.save(documents);
		return documents;
	}

	@Override
	public int delete(Documents documents) {
		docRep.delete(documents);
		return 1;
	}

	@Override
	public int deleteById(int id) {
		docRep.deleteById(id);
		return 1;
	}

	@Override
	public Documents update(Documents documents, int id) {
		documents.setUser(docRep.findById(id).get().getUser());
		documents.setDocumentId(id);
		docRep.save(documents);
		return documents;
	}

	@Override
	public List<Documents> fetchAll() {
		return (List<Documents>) docRep.findAll();
	}

	@Override
	public Documents fetchByUser(AppUser appUser) {
		return appUser.getDocuments();
	}


}
