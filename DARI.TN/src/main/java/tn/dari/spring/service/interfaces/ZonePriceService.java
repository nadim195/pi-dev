package tn.dari.spring.service.interfaces;

import java.util.List;

import tn.dari.spring.domain.ZonePrice;

public interface ZonePriceService {

	public ZonePrice add(ZonePrice zonePrice);
	public int delete(ZonePrice zonePrice);
	public int deleteById(int id);
	public ZonePrice update(ZonePrice zonePrice, int id);
	public List<ZonePrice> fetchAll();

}
