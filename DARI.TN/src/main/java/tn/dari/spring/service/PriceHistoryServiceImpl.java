package tn.dari.spring.service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.PriceHistory;
import tn.dari.spring.domain.util.AnnouncementInfo;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.PriceHistoryRepository;
import tn.dari.spring.service.interfaces.NotificationService;
import tn.dari.spring.service.interfaces.PriceHistoryService;

@Service
public class PriceHistoryServiceImpl implements PriceHistoryService {
	@Autowired
	PriceHistoryRepository priceHistoryRep;
	@Autowired
	AnnouncementRepository annRep;
	@Autowired
	NotificationService notifService;

	//local function
	//Difference between current priceHistory and last Price of announcement
	private double priceDifference(Announcement announcement, PriceHistory priceHistory){
		Set<PriceHistory> histories = priceHistoryRep.findByAnnouncement(announcement);
		double lastPrice = histories.stream().findFirst().get().getPrice();
		Date lastDate = histories.stream().findFirst().get().getUpdateDate();
		for (PriceHistory ph : histories) {
			if(lastDate.before(ph.getUpdateDate())){
				lastDate = ph.getUpdateDate();
				lastPrice = ph.getPrice();
			}
		}
		return priceHistory.getPrice()-lastPrice;
	}
	
	//Call Every Time you Add PriceHistory, right before calculating difference and saving
	//Deletes other History entries on same day as parameter
	//Keeps only one entry per announcement per day
	private void deleteEntriesOfSameDay(PriceHistory priceHistory, Announcement announcement){
		boolean t=true;
		PriceHistory ph;
		do{
			//ph = priceHistoryRep.retrieveLastByAnnouncement(announcement);
			ph = priceHistoryRep.findByAnnouncement(announcement).stream().sorted((ph1,ph2)-> ph2.getUpdateDate().compareTo(ph1.getUpdateDate())).findFirst().get();
			if(LocalDate.from(Instant.ofEpochMilli(ph.getUpdateDate().getTime())).equals(LocalDate.from(Instant.ofEpochMilli(priceHistory.getUpdateDate().getTime()))))
				priceHistoryRep.delete(ph);
			else
				t=false;
		}while(t);
	}
	
	//Announcement Price History of the Last (Custom Period)
	@Override
	public Set<PriceHistory> fetchByPeriod(Announcement announcement, Period period){
		return priceHistoryRep.findByUpdateDateAfter(Date.from(Instant.now().minus(period))).stream().filter(p -> p.getAnnouncement().equals(announcement)).collect(Collectors.toSet());
	}
	
	//DoubleSummaryStatistics(min, max, avg...) of period
	@Override
	public DoubleSummaryStatistics getSummaryStatisticsOfPeriod(Announcement announcement, Period period){
		return fetchByPeriod(announcement,period).stream().mapToDouble(mapper -> mapper.getPrice()).summaryStatistics();
	}
	
	//return announcementInfo Object
	@Override
	public AnnouncementInfo getAnnouncementInfo(Announcement announcement, Period period){
		DoubleSummaryStatistics doubleSummaryStatistics = getSummaryStatisticsOfPeriod(announcement, period);
		Set<PriceHistory> priceHistories = fetchByPeriod(announcement, period);
		AnnouncementInfo announcementInfo = new AnnouncementInfo(announcement.getAnnouncementId(), announcement.getType(), announcement.getEstateType(), announcement.getLocation(), announcement.getPrice(), announcement.getDescription(), announcement.getSurface(), announcement.getNumberOfFloors(), announcement.getNumberOfRooms(), announcement.getDateCreated(), announcement.getDatePublished(),doubleSummaryStatistics,priceHistories);
		return announcementInfo;
	}
	

	
	//Should be called every Announcement Add or Update
	//Adds PriceHistory entry, links it with given announcement, Creates Notifications and returns Price Difference
	@Override
	public double announcementPriceUpdate(Announcement announcement) {
		PriceHistory priceHistory = new PriceHistory();

		priceHistory.setAnnouncement(announcement);
		priceHistory.setUpdateDate(Date.from(Instant.now()));
		priceHistory.setPrice(announcement.getPrice());
		
		if(priceHistoryRep.findByAnnouncement(announcement).isEmpty()){
			priceHistoryRep.save(priceHistory);
			return 0;
		}
		deleteEntriesOfSameDay(priceHistory,announcement);
		double priceDiff = priceDifference(announcement,priceHistory);
		priceHistoryRep.save(priceHistory);
		notifService.create(priceDiff, announcement.getAnnouncementId());
		return priceDiff;
	}
	
	//List of the history of the price of an announcement
	@Override
	public List<PriceHistory> fetchAllByAnnouncement(Announcement announcement) {
		return announcement.getPriceHistories().stream().collect(Collectors.toList());
	}
	
	
	//Adds PriceHistory entry, links it with given announcement and returns Price Difference
	//And creates notifications
	//For testing only
	@Override
	public double add(PriceHistory priceHistory, int announcementId) {
		Announcement announcement = annRep.findById(announcementId).get();
		priceHistory.setAnnouncement(announcement);
		priceHistory.setUpdateDate(Date.from(Instant.now()));
		deleteEntriesOfSameDay(priceHistory,announcement);
		double priceDiff = priceDifference(announcement,priceHistory);
		priceHistoryRep.save(priceHistory);
		notifService.create(priceDiff, announcementId);
		return priceDiff;
	}
	
	
	//For testing only
	@Override
	public PriceHistory update(PriceHistory priceHistory, int id) {
		PriceHistory ph = priceHistoryRep.findById(id).get();
		priceHistory.setAnnouncement(ph.getAnnouncement());
		priceHistory.setId(id);
		priceHistoryRep.save(priceHistory);
		return priceHistory;
	}
	
	//For testing only
	@Override
	public int delete(PriceHistory priceHistory) {
		priceHistoryRep.delete(priceHistory);
		return 1;
	}

	//For testing only
	@Override
	public int deleteById(int id) {
		priceHistoryRep.deleteById(id);
		return 1;
	}

	//For testing only
	@Override
	public List<PriceHistory> fetchAll() {
		return (List<PriceHistory>) priceHistoryRep.findAll();
	}

}
