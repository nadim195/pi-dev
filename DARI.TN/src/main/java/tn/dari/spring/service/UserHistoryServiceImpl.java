package tn.dari.spring.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.dari.spring.domain.Announcement;
import tn.dari.spring.domain.AppUser;
import tn.dari.spring.repository.AnnouncementRepository;
import tn.dari.spring.repository.AppUserRepository;
import tn.dari.spring.service.interfaces.UserHistoryService;

@Service
public class UserHistoryServiceImpl implements UserHistoryService {
	@Autowired
	AppUserRepository appRep;
	@Autowired
	AnnouncementRepository annRep;

	@Override
	public void addToHistory(int iduser,int idann) {
		AppUser user=appRep.findById(iduser).get();
		Announcement a=annRep.findById(idann).get();
		if(a==null) throw new RuntimeException("Announcement not found");
		if(user==null) throw new RuntimeException("user not found");
		Set<Announcement> favs = new HashSet<Announcement>();
		favs=user.getAnnouncementsHistory();
		favs.add(a);
		user.setAnnouncementsHistory(favs);
		appRep.save(user);
	}

	@Override
	public void deleteFromHistory(int iduser, int idann) {
		AppUser user=appRep.findById(iduser).get();
		Announcement a=annRep.findById(idann).get();
		if(a==null) throw new RuntimeException("Announcement not found");
		if(user==null) throw new RuntimeException("user not found");
		Set<Announcement> favs = new HashSet<Announcement>();
		favs=user.getAnnouncementsHistory();
		favs.remove(a);
		user.setAnnouncementsHistory(favs);
		appRep.save(user);
		
	}

	@Override
	public Set<Announcement> fetchAllhistory(int iduser) {
		AppUser user=appRep.findById(iduser).get();
		if(user==null) throw new RuntimeException("user not found");
		return  user.getAnnouncementsHistory();
	}

}
