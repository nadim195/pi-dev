package tn.dari.spring.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import tn.dari.spring.domain.Credit;
import tn.dari.spring.dto.CreditDTO;

@Mapper(componentModel = "spring", uses = ReferenceMapper.class)
public interface CreditMapper extends GenericMapper<Credit, CreditDTO> {
    @Override
    @Mapping(target = "id", ignore = false)
    @Mapping(target = "user", ignore = true)
    Credit asEntity(CreditDTO dto);

    @Mapping(target = "user", source = "user.email")
    @Mapping(target = "bankName", source = "bankSpecification.bankName")
    CreditDTO asDTO(Credit entity);
}
